package ntnu.idatt2001.k2_04.to_doListApp.category;

import javafx.collections.ObservableList;
import ntnu.idatt2001.k2_04.to_doListApp.category.Category;
import ntnu.idatt2001.k2_04.to_doListApp.mainTask.MainTask;
import ntnu.idatt2001.k2_04.to_doListApp.user.User;

import java.io.IOException;
import java.util.ArrayList;

/**
 * A Dao interface for Category
 */
public interface CategoryDao {
    void addCategory(String categoryName) throws Exception;
    void deleteCategory(String categoryName) throws Exception;
    int getCategoryID(String categoryName) throws Exception;
    Category getCategoryByName(String categoryName) throws Exception;
    ObservableList<MainTask> getAllMainTasksInCategory(int categoryID) throws Exception;
    ArrayList<MainTask> getAllMainTaskDoneInCategory() throws IOException;
}
