package ntnu.idatt2001.k2_04.to_doListApp.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import ntnu.idatt2001.k2_04.to_doListApp.mainTask.MainTaskInfo;
import ntnu.idatt2001.k2_04.to_doListApp.SceneChanger;
import ntnu.idatt2001.k2_04.to_doListApp.mainTask.MainTask;


import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.*;

/**
 * A controller class for the homeWithoutLogin.fxml.
 */
public class HomeWithoutLoginController implements Initializable {

    private SceneChanger sceneChanger = new SceneChanger();

    // Buttons
    @FXML private Button addTaskButton;
    @FXML private Button deleteTasksButton;
    @FXML private Button showFinishedTasksButton;
    @FXML private Button backToLoginButton;
    @FXML private Button markTasksAsFinishedButton;

    //These instance variables/fields are used for creating new task-objects
    @FXML private TextField taskNameField;
    @FXML private ComboBox priorityComboBox;
    @FXML private DatePicker deadline;

    // TableView
    @FXML private TableView<MainTask> tableView;
    @FXML private TableColumn<MainTask, String> nameColumn;
    @FXML private TableColumn<MainTask, CheckBox> doneColumn;
    @FXML private TableColumn<MainTask, ComboBox> priorityColumn;
    @FXML private TableColumn<MainTask, LocalDate> deadlineColumn;

    //Initialize method for tableview, buttons and ComboBox
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        //setting up the columns for the table
        nameColumn.setCellValueFactory(new PropertyValueFactory<MainTask, String>("taskName"));
        doneColumn.setCellValueFactory(new PropertyValueFactory<MainTask, CheckBox>("taskDone"));
        priorityColumn.setCellValueFactory(new PropertyValueFactory<MainTask, ComboBox>("taskPriority"));
        deadlineColumn.setCellValueFactory(new PropertyValueFactory<MainTask, LocalDate>("deadline"));


        //Load in data
        tableView.setItems(MainTaskInfo.getUnfinishedMainTasksNoLogin());

        //Updating the table to allow user to edit 'Task Name'-column and 'Priority'-column
        tableView.setEditable(true);
        nameColumn.setCellFactory(TextFieldTableCell.forTableColumn());


        //this will allow the user to select multiple tasks a
        tableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);


        //Disabling deleteButton until MainTask is selected in tableview
        deleteTasksButton.setDisable(true);

        //Setting values for ComboBox containing priorities
        ObservableList<Integer> priorities = FXCollections.observableArrayList(1, 2, 3);
        priorityComboBox.setItems(priorities);
    }

    /**
     * When backToLoginButton is clicked, this method takes the user back to the login page.
     */
    @FXML
    public void backToLoginButtonClicked(ActionEvent event) throws IOException {
        sceneChanger.changeScene(event, "login.fxml", "Login (to-do list application)");
    }

    /**
     * Method that enables delete button when task(s) are/is selected
     */
    @FXML
    public void enableDeleteButton() {
        deleteTasksButton.setDisable(false);
    }


    /**
     * This method takes the selected task and deletes it from the tableview
     */
    @FXML
    public void deleteMainTask(){
        ObservableList<MainTask> selectedRows, allRows;
        allRows = tableView.getItems();
        selectedRows = tableView.getSelectionModel().getSelectedItems();
        allRows.removeAll(selectedRows);
    }

    /**
     * Method removes elements in ObservableList of selected tasks from ObservableList of all tasks
     */
    @FXML
    public void deleteButtonClicked() {
        deleteMainTask();
    }

    /**
     * This method will create a new Task-object and add it to the table
     * Clears out fields after
     */
    @FXML
    public void addTaskButtonClicked(){
        MainTask newTask = new MainTask(taskNameField.getText(),
                (Integer) priorityComboBox.getValue(),deadline.getValue());
        //tableView.getItems().add(newTask);
        MainTaskInfo.addUnfinishedMainTaskNoLogin(newTask);

        taskNameField.clear();
        priorityComboBox.getEditor().clear();
        deadline.getEditor().clear();
    }

    /**
     * Method for the event of editing a task name
     */
    @FXML
    public void editTaskName(CellEditEvent editedCell){
        MainTask mainTaskSelected = tableView.getSelectionModel().getSelectedItem();
        mainTaskSelected.setTaskName(editedCell.getNewValue().toString());
    }

    /**
     * Marks selected MainTask as done
     * Sets end-date of selected MainTask to now
     * Uses MainTaskInfo to add the task to a static ObservableList of finished tasks
     * This list will be loaded into the view of FinishedTaskController
     * @param //taskDone is the CellEvent of checking a task as done
     */
    @FXML
    public ObservableList<MainTask> getSelectedCheckboxes(){
        ObservableList<MainTask> tasksDone = FXCollections.observableArrayList();
        try{
            for(MainTask mainTaskSelected : tableView.getItems()){
                if(mainTaskSelected.getTaskDone().isSelected()){
                    tasksDone.add(mainTaskSelected);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return tasksDone;
    }

    /**
     * When the markAsFinishedButton is clicked, this method marks all the selected tasks
     * as finished, and deleted them from the tableview.
     */
    @FXML
    public void markAsFinishedButtonClicked(){
        try {
            for(MainTask mainTask : getSelectedCheckboxes()){
                mainTask.finishTask();
                //mainTask.setEndDate(LocalDateTime.now());
            }
            MainTaskInfo.setFinishedMainTasksNoLogin(getSelectedCheckboxes());
            deleteMainTask();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * When showFinishedTasksButton is clicked,
     * user is taken to the finishedTask.fxml page, where it can see all the
     * finished tasks.
     */
    @FXML
    public void showFinishedTasksButtonClicked(ActionEvent event) throws IOException {
        sceneChanger.changeScene(event, "finishedTasks.fxml", "Finished tasks");
    }
}
