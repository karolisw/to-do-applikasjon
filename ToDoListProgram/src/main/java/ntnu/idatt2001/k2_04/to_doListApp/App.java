package ntnu.idatt2001.k2_04.to_doListApp;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import ntnu.idatt2001.k2_04.to_doListApp.category.CategoryDao;
import ntnu.idatt2001.k2_04.to_doListApp.category.JdbcCategoryDao;
import ntnu.idatt2001.k2_04.to_doListApp.mainTask.JdbcMainTaskDao;
import ntnu.idatt2001.k2_04.to_doListApp.mainTask.MainTaskDao;
import ntnu.idatt2001.k2_04.to_doListApp.subTask.JdbcSubTaskDao;
import ntnu.idatt2001.k2_04.to_doListApp.subTask.SubTaskDao;
import ntnu.idatt2001.k2_04.to_doListApp.user.JdbcUserDao;
import ntnu.idatt2001.k2_04.to_doListApp.user.UserDao;

import java.io.FileInputStream;

/**
 * JavaFX App
 */
public class App extends Application {
    private static Stage window;



    public static void main(String[] args) {
        launch();
    }


    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("login.fxml"));

        //giving stage a more appropriate name
        window = primaryStage;
        window.setTitle("ToDo-application");

        window.setResizable(false);
        Image icon = new Image("checkListIcon.png");
        window.getIcons().add(icon);

        window.setScene(new Scene(root,400,300));
        window.show();

    }
}