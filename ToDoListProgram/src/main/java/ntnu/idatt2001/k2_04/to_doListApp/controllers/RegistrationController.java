package ntnu.idatt2001.k2_04.to_doListApp.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import ntnu.idatt2001.k2_04.to_doListApp.SceneChanger;
import ntnu.idatt2001.k2_04.to_doListApp.category.Category;
import ntnu.idatt2001.k2_04.to_doListApp.category.CategoryDao;
import ntnu.idatt2001.k2_04.to_doListApp.category.JdbcCategoryDao;
import ntnu.idatt2001.k2_04.to_doListApp.user.JdbcUserDao;
import ntnu.idatt2001.k2_04.to_doListApp.user.User;
import ntnu.idatt2001.k2_04.to_doListApp.user.UserDao;
import ntnu.idatt2001.k2_04.to_doListApp.user.UserInfo;
import ntnu.idatt2001.k2_04.to_doListApp.user.UserInfo;

import java.io.IOException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.util.ResourceBundle;

/**
 * A controller class for registering a new user (registration.fxml).
 */
public class RegistrationController implements Initializable{

    // For changing scenes without messy code
    private SceneChanger sceneChanger = new SceneChanger();
    private CategoryDao categoryDao = new JdbcCategoryDao();
    private UserDao userDao = new JdbcUserDao();
    private User user;

    // TextFields
    @FXML private TextField firstNameField;
    @FXML private TextField lastNameField;
    @FXML private TextField usernameField;
    @FXML private PasswordField passwordField;
    @FXML private PasswordField confirmPasswordField;

    // CheckBox
    @FXML private CheckBox gdprCheckBox;

    // Label
    @FXML private Label registrationSuccessLabel;
    @FXML private Label passwordSuccessLabel;
    @FXML private Label gdprLabel;

    // ToolTips
    Tooltip characters = new Tooltip("Min: 6 characters");
    Tooltip password = new Tooltip("Password fields must be equal");

    // Setting ToolTips
    public void setToolTips(){
        usernameField.setTooltip(characters);
        passwordField.setTooltip(password);
        confirmPasswordField.setTooltip(password);
    }
    /**
     * On action for when cancelButton is clicked
     * We want to return to our login screen
     */
    public void cancelButtonClicked(ActionEvent event) throws IOException {
        sceneChanger.changeScene(event, "login.fxml","Login (To-do List application)");
    }

    public void cleanLabels(){
        registrationSuccessLabel.setText("");
        passwordSuccessLabel.setText("");
    }

    /**
     * Check for if no TextFields are blank
     */
    public boolean noEmptyTextFields(){
        if ((firstNameField.getText().isBlank() || lastNameField.getText().isBlank() || usernameField.getText().isBlank()
                || passwordField.getText().isBlank() || confirmPasswordField.getText().isBlank())){
            registrationSuccessLabel.setText("Please fill in all text fields");
            throw new IllegalArgumentException();        }
        else return true;
    }

    // Check for if the password-fields are equal
     public boolean passwordFieldsAreEqual(){
         if (passwordField.getText().equals(confirmPasswordField.getText())){
             return true;
         }
         else {
             passwordSuccessLabel.setText("Password fields not equal");
             throw new IllegalArgumentException();         }
     }

     // The checkbox must be pushed
    public boolean consentCheckBoxClicked(){
        if(gdprCheckBox.isSelected()){
            return true;
        }
        else {
            registrationSuccessLabel.setText("You must consent or use the app without a user");
            throw new IllegalArgumentException();
        }
    }

    /**
     * Method that melts all checks for input into one line of code
     */
    public boolean validInput(){
        if(noEmptyTextFields() && passwordFieldsAreEqual() && consentCheckBoxClicked()){
            return true;
        }else {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Method for constructing a user with the information in fields
     * Because we need the salt to register user, and the salt gets created along with the user
     * @Exception NoSuchAlgorithmException because the constructor uses SHA-512 for constructing salt
     */
    public void createUser() throws NoSuchAlgorithmException {
        user = new User(firstNameField.getText(), lastNameField.getText(), usernameField.getText(), passwordField.getText(),0);
    }

    /**
     * Registration button event validates input and adds new user to db
     * Lastly sets static field user (in singleton class UserInfo) to the current user
     * for easier flow with db
     * @param event button clicked
     * @throws IllegalArgumentException
     */
    public void registrationButtonClicked(ActionEvent event) {
        cleanLabels();
        if(validInput()){
            try {
                createUser();
                userDao.addUserToDB(firstNameField.getText(), lastNameField.getText(),
                        usernameField.getText(), passwordField.getText(),user.getSalt());

                User userFromDB = userDao.getUser(usernameField.getText());
                UserInfo.initializeUser(userFromDB);
                assignStandardCategoriesToUser();

                registrationSuccessLabel.setText("User has been registered successfully!");
                sceneChanger.changeScene(event,"login.fxml","User login");
            }catch (IllegalArgumentException | IOException | NoSuchAlgorithmException e){
                e.getCause();
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Assigns the newly created user the default categories
     */
    public void assignStandardCategoriesToUser(){
        try{
            userDao.setDefaultCategoriesNewUser();

        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        setToolTips();
    }
}


