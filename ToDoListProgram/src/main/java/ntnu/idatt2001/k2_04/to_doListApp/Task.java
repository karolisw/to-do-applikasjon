package ntnu.idatt2001.k2_04.to_doListApp;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.CheckBox;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.chrono.ChronoLocalDate;
import java.time.chrono.ChronoLocalDateTime;

/**
 * Task class, an abstract class
 */

public abstract class Task {
    private boolean done;
    private LocalDate deadline;
    private LocalDate startDate;
    private LocalDate endDate;
    private int id;


    /**
     * Field implementation of SimpleStringProperty,
     * SimpleIntegerProperty and CheckBox.
     * For use in constructor resembling Task-objects in
     * Home-page TableView
     */
    private SimpleStringProperty taskName;
    private SimpleIntegerProperty taskPriority;
    private CheckBox taskDone;

    /**
     * Accessor -and mutator methods especially made to get and set
     * SimpleStringProperty, SimpleIntegerProperty and CheckBox
     */


    public String getTaskName() {
        return taskName.get();
    }

    public void setTaskName(String taskName) {
        this.taskName = new SimpleStringProperty(taskName);
    }

    public int getTaskPriority() {
        return taskPriority.get();
    }

    public void setTaskPriority(int taskPriority) {
        this.taskPriority = new SimpleIntegerProperty(taskPriority);
    }

    public CheckBox getTaskDone() {
        return taskDone;
    }

    public void setTaskDone(CheckBox taskDone) {
        this.taskDone = taskDone;
        this.done = true;
    }


    /**
     * Constructor for tableView objects implementing property-fields
     * @param taskName String converted to SimpleStringProperty
     * @param deadline LocalDate
     * @param priority int converted to SimpleIntegerProperty
     */
    public Task(String taskName, int priority, LocalDate deadline, boolean done, LocalDate startDate, LocalDate endDate, int id){
        this.taskName = new SimpleStringProperty(taskName);
        this.taskPriority = new SimpleIntegerProperty(priority);
        this.deadline = deadline;
        this.taskDone = new CheckBox();
        this.done = done;
        this.startDate = startDate;
        this.endDate = endDate;
        this.id = id;
    }

    /**
     * Constructor including all object variables.
     * @param taskName The name of the task.
     * @param priority  An integer from 0 (low priority) to 2 (high priority).
     * @param deadline The date the task should be finished.
     * Used in HomeWithoutLogin
     */
    public Task(String taskName, int priority, LocalDate deadline) {
        this.taskName = new SimpleStringProperty(taskName);
        this.taskPriority = new SimpleIntegerProperty(priority);
        this.deadline = deadline;
        this.taskDone = new CheckBox();
    }

    /**
     * Accessor- and mutator-methods
     */

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public LocalDate getDeadline() {
        return deadline;
    }

    public void setDeadline(LocalDate deadline) {
        this.deadline = deadline;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void finishTask() { // TODO Merge methods finishTask and undoTask
        setDone(true);
        setEndDate(LocalDate.now());
    }

    public void undoTask() {
        setDone(false);
        setEndDate(null); //TODO Does this work?
    }

    @Override
    public String toString() {
        return "Task: \n" + getTaskName() +
                "\nPriority: " + getTaskPriority() +
                "\nDone: " + done +
                "\nDeadline: " + deadline +
                "\nDuration: " + startDate + " - " + endDate;
    }
}