package ntnu.idatt2001.k2_04.to_doListApp.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import ntnu.idatt2001.k2_04.to_doListApp.HintPopup;
import ntnu.idatt2001.k2_04.to_doListApp.SceneChanger;
import ntnu.idatt2001.k2_04.to_doListApp.user.JdbcUserDao;
import ntnu.idatt2001.k2_04.to_doListApp.user.User;
import ntnu.idatt2001.k2_04.to_doListApp.user.UserDao;
import ntnu.idatt2001.k2_04.to_doListApp.user.UserInfo;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * A controller class for the login.fxml page.
 */
public class LoginController implements Initializable {

    /**
     * This field contains the users username and password (if they click log in)
     * We bring this User-object with us as we move through the app for easy db access
     * this is because we want to know which user is currently logged in for many reasons
     */
    private User user;
    private SceneChanger sceneChanger = new SceneChanger();
    private UserDao userDao = new JdbcUserDao();

    // Labels
    @FXML private Label usernameLabel;
    @FXML private Label passwordLabel;
    @FXML private Label wrongLogin;

    // TextField
    @FXML private TextField usernameTextField;

    // PasswordField
    @FXML private PasswordField passwordField;

    // Button
    @FXML private Button loginButton;
    @FXML private Button registerNewUserButton;
    @FXML private Button useWithoutAccountButton;

    // ImageView
    @FXML private ImageView lockImageView;
    @FXML private ImageView toDoAppImageView;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
    }

    /**
     * Method for the event of when user clicks login button
     * Login is verified (db is checked)
     * Values username and password are put into user-field
     * After login, the user changes scene to "Home.fxml", where their tasks are displayed
     * throws exception
     */
    public void loginButtonClicked(ActionEvent event) throws Exception {
        if (!usernameTextField.getText().isBlank() && !passwordField.getText().isBlank()) {
            if (userDao.verifyLogin(usernameTextField.getText(), passwordField.getText())) {
                setCurrentUser();
                UserInfo.initializeUser(user);
                sceneChanger.changeScene(event,"Home.fxml", "Home");
                HintPopup.display();

            } else {
                wrongLogin.setText("Incorrect username or password");
            }
        } else if (usernameTextField.getText().isEmpty() || passwordField.getText().isEmpty()) {
            wrongLogin.setText("Please enter username and password");
        }
    }

    /**
     * If the user wants to use the application without logging in (withoout connecting to db),
     * this method takes them to homeWithoutLogin page.
     */
    public void useWithoutAccountButtonClicked(ActionEvent event) throws IOException {
        sceneChanger.changeScene(event, "homeWithoutLogin.fxml", "To-do list");
    }

    /**
     * Method for retrieving the user-credentials from the log in form
     */
    public void setCurrentUser() throws Exception {
        this.user = userDao.getUser(usernameTextField.getText());
    }

    /**
     * This method will move the user to a new scene with the registration view
     */
     public void registerNewUserButtonClicked(ActionEvent event) throws IOException {
         sceneChanger.changeScene(event, "registration.fxml", "Registration form");
     }

}
