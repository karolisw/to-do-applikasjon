package ntnu.idatt2001.k2_04.to_doListApp.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import ntnu.idatt2001.k2_04.to_doListApp.mainTask.MainTaskInfo;
import ntnu.idatt2001.k2_04.to_doListApp.SceneChanger;
import ntnu.idatt2001.k2_04.to_doListApp.subTask.JdbcSubTaskDao;
import ntnu.idatt2001.k2_04.to_doListApp.subTask.SubTaskDao;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class AddSubTaskController implements Initializable {
    private SubTaskDao subTaskDao = new JdbcSubTaskDao();
    //Object-variables
    private SceneChanger sceneChanger = new SceneChanger();

    //GUI instances
    @FXML private TextField nameField;
    @FXML private DatePicker deadlineField;
    @FXML private ComboBox<Integer> prioritiesCombobox;
    @FXML private Button createButton;
    @FXML private Button cancelButton;

    //A list with all the priorities for the combobox
    ObservableList<Integer> prioritiesList = FXCollections.observableArrayList(1,2,3);

    public void initialize(URL location, ResourceBundle resources){
        //Sets the priorities in the combobox
        prioritiesCombobox.setItems(prioritiesList);
    }

    /**
     * A method for changing to showTaskDeatil.fxml when cancel is clicked
     * @param event ActionEvent when cancelButton clicked
     * @throws IOException throws exception if any error is occurring
     */
    @FXML
    public void cancelClicked(ActionEvent event) throws IOException {
        MainTaskInfo.removeMainTask();
        sceneChanger.changeScene(event,"showTaskDetails.fxml","Task details");
    }


    /**
     * A method for when create is clicked. It adds the subtask to the
     * tableview in show task details page.
     */
    @FXML
    public void createClicked(ActionEvent event) throws Exception {
        try {
            subTaskDao.addNewSubTask(nameField.getText(), prioritiesCombobox.getValue(), deadlineField.getValue(), MainTaskInfo.getMainTaskID());
            sceneChanger.changeScene(event,"showTaskDetails.fxml","Task details");
        } catch (IOException ex) {
            System.err.println(ex);
        }
    }
}
