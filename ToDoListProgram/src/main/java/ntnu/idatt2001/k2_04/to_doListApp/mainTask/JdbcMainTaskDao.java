package ntnu.idatt2001.k2_04.to_doListApp.mainTask;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import ntnu.idatt2001.k2_04.to_doListApp.DatabaseConnection;
import ntnu.idatt2001.k2_04.to_doListApp.category.CategoryDao;
import ntnu.idatt2001.k2_04.to_doListApp.category.CategoryInfo;
import ntnu.idatt2001.k2_04.to_doListApp.category.JdbcCategoryDao;
import ntnu.idatt2001.k2_04.to_doListApp.subTask.JdbcSubTaskDao;
import ntnu.idatt2001.k2_04.to_doListApp.subTask.SubTaskDao;
import ntnu.idatt2001.k2_04.to_doListApp.category.Category;
import ntnu.idatt2001.k2_04.to_doListApp.user.JdbcUserDao;
import ntnu.idatt2001.k2_04.to_doListApp.user.UserDao;
import ntnu.idatt2001.k2_04.to_doListApp.user.UserInfo;

import java.io.IOException;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 * A class that allows a connection to the database to access the information about a MainTask stored there.
 */
public class JdbcMainTaskDao implements MainTaskDao {
    private SubTaskDao subTaskDao;
    private UserDao userDao;
    private CategoryDao categoryDao;

    /**
     * A method that adds a MainTask to the database.
     * @param title the title of the task to be added
     * @param priority the priority of the task to be added
     * @param deadline the deadline of the task to be added
     * @param description the description of the task to be added
     * @param categoryID a unique ID for the category to distinguish from other categories stored in the database
     * @throws Exception if unable to connect to the database
     */
    public void addMainTask(String title, int priority, LocalDate deadline, String description, int categoryID) throws Exception {
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();

        try {
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM maintask WHERE title=? AND categoryID=?");
            ps.setString(1, title);
            ps.setInt(2, categoryID);
            ResultSet rs = ps.executeQuery();
            if(!rs.next()) {
                PreparedStatement prepS = connection.prepareStatement("INSERT INTO maintask(title,priority,done,deadline,startdate,description,categoryID,stringOfSubTask) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
                prepS.setString(1, title);
                prepS.setInt(2, priority);
                prepS.setBoolean(3, false);
                prepS.setString(4, deadline.toString());
                prepS.setString(5, (LocalDate.now()).toString());
                prepS.setString(6, description);
                prepS.setInt(7, categoryID);
                prepS.setString(8, " ");
                prepS.execute();
            } else {
                throw new IllegalArgumentException("Maintask with this title already exists in selected category.");
            }
        }
        catch (SQLException e) {
            throw new Exception("Unable to create maintask " + e.getMessage());
        }
        finally {
            databaseConnection.closeConnection();
        }
    }

    /**
     * A method that deletes a MainTask from the database.
     * @throws Exception if unable to connect to the database
     */
    public void deleteMainTask(int mainTaskID) throws Exception {
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();

        subTaskDao = new JdbcSubTaskDao();
        subTaskDao.deleteAllSubTaskFromMainTask(mainTaskID);

        try {
            PreparedStatement ps = connection.prepareStatement("DELETE FROM maintask WHERE maintaskID=?");
            ps.setInt(1, mainTaskID);
            ps.execute();
        }
        catch (SQLException e) {
            throw new Exception("Unable to delete maintask : " + e.getMessage());
        }
        finally {
            databaseConnection.closeConnection();
        }
    }

    /**
     * A method that deletes all the MainTasks belonging to a category.
     * @param category the category that holds all the MainTasks to be deleted
     * @throws Exception if unable to connect to the database
     */
    public void deleteAllMainTasksInCategory(Category category) throws Exception {
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();

        try {
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM maintask WHERE categoryID=?");
            ps.setInt(1, category.getCategoryID());
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                subTaskDao.deleteAllSubTaskFromMainTask(rs.getInt("maintaskID"));
            }
            PreparedStatement prepS = connection.prepareStatement("DELETE FROM maintask WHERE categoryID=?");
            prepS.setInt(1, category.getCategoryID());
            prepS.execute();
        }
        catch (SQLException e) {
            throw new Exception("Unable to delete maintasks in categories " + category.getCategoryName() + ": " + e.getMessage());
        }
        finally {
            databaseConnection.closeConnection();
        }
    }

    /**
     * A method that moves the MainTasks in a category to the Uncategorized category. For instance when a user deletes
     * a category.
     * @param category the category that stores the MainTasks to be moved
     * @throws Exception if unable to connect to the database
     */
    public void changeToUncategorizedForMainTasksInCategory(Category category) throws Exception {
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        categoryDao = new JdbcCategoryDao();
        try {
            PreparedStatement prepS = connection.prepareStatement("UPDATE maintask SET categoryID=? WHERE categoryID=?");
            prepS.setInt(1, categoryDao.getCategoryID("uncategorized"));
            prepS.setInt(2, categoryDao.getCategoryID(category.getCategoryName()));
            prepS.execute();
        }
        catch (SQLException e) {
            throw new Exception("Unable to move maintasks from category " + category.getCategoryName() + " to uncategorized : " + e.getMessage());
        }
        finally {
            databaseConnection.closeConnection();
        }
    }

    /**
     * A method that updates the name of a MainTask
     * @param newTaskName the new name of the task
     * @throws Exception if unable to connect to the database
     */
    public void updateTaskName(String newTaskName) throws Exception {
        DatabaseConnection connection = new DatabaseConnection();
        Connection connectDB = connection.getConnection();

        try {
            PreparedStatement ps = connectDB.prepareStatement("UPDATE maintask SET title=? WHERE maintaskID=?");
            ps.setString(1, newTaskName);
            ps.setInt(2, MainTaskInfo.getMainTaskID());
            ps.execute();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            connection.closeConnection();
        }
    }

    /**
     * A method that updates the priority of a MainTask.
     * @param newPriority the new priority of the task
     * @throws Exception if unable to connect to the database
     */
    public void updatePriority(int newPriority) throws Exception {
        DatabaseConnection connection = new DatabaseConnection();
        Connection connectDB = connection.getConnection();

        try {
            PreparedStatement ps = connectDB.prepareStatement("UPDATE maintask SET priority=? WHERE maintaskID=?");
            ps.setInt(1, newPriority);
            ps.setInt(2, MainTaskInfo.getMainTaskID());
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            connection.closeConnection();
        }
    }

    /**
     * A method that updates the description of a MainTask.
     * @param description the new description of the MainTask
     * @throws IOException if unable to connect to the database
     */
    public void updateDescription(String description) throws IOException {
        DatabaseConnection connection = new DatabaseConnection();
        Connection connectDB = connection.getConnection();

        try {
            PreparedStatement ps = connectDB.prepareStatement("UPDATE maintask SET description=? WHERE maintaskID=?");
            ps.setString(1, description);
            ps.setInt(2, MainTaskInfo.getMainTaskID());
            ps.execute();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            connection.closeConnection();
        }
    }

    /**
     * A method that updates the deadline of a MainTask.
     * @param deadline the new deadline
     * @throws IOException if unable to connect to the database
     */
    public void updateDeadline(LocalDate deadline) throws IOException {
        DatabaseConnection connection = new DatabaseConnection();
        Connection connectDB = connection.getConnection();
        Date date = Date.valueOf(deadline);

        try {
            PreparedStatement ps = connectDB.prepareStatement("UPDATE maintask SET deadline=? WHERE maintaskID=?");
            ps.setString(1, String.valueOf(date));
            ps.setInt(2, MainTaskInfo.getMainTaskID());
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            connection.closeConnection();
        }
    }

    /**
     * Method loads maintasks in from db
     * Maintasks are inserted into tableView
     * @throws SQLException
     */
    public ObservableList<MainTask> loadMainTasks() throws IOException {
        ObservableList<MainTask> mainTasksObservableList = FXCollections.observableArrayList();
        userDao = new JdbcUserDao();
        categoryDao = new JdbcCategoryDao();
        try {
            ObservableList<Category> categoriesForUser = userDao.allCategoriesForUser(UserInfo.getUser().getId());
            for(Category category : categoriesForUser){
                if(!category.getCategoryName().equals("all")) {
                    ObservableList<MainTask> mainTasks = categoryDao.getAllMainTasksInCategory(categoryDao.getCategoryID(category.getCategoryName()));
                    mainTasksObservableList.addAll(mainTasks);
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        } return mainTasksObservableList;
    }

    /**
     * Set maintask as done
     * @throws IOException if unable to set task as done
     */
    public void setDone() throws IOException {
        DatabaseConnection connection = new DatabaseConnection();
        Connection connectDB = connection.getConnection();

        try {
            PreparedStatement ps = connectDB.prepareStatement("UPDATE maintask SET done=true WHERE maintaskID=?");
            ps.setInt(1, MainTaskInfo.getMainTaskID());
            ps.executeUpdate();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            connection.closeConnection();
        }
    }

    /**
     * A method what sets the enddate to the current time.
     * @throws IOException if unable to do so
     */
    public void setEndDate() throws IOException {
        DatabaseConnection connection = new DatabaseConnection();
        Connection connectDB = connection.getConnection();

        try {
            PreparedStatement ps = connectDB.prepareStatement("UPDATE maintask SET enddate=? WHERE maintaskID=?");
            ps.setString(1, String.valueOf(LocalDate.now()));
            ps.setInt(2, MainTaskInfo.getMainTaskID());
            ps.executeUpdate();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            connection.closeConnection();
        }
    }

    /**
     * Method takes in name of category to be found, and the id of the user who owns the category
     * Method is used to set category for a maintask when it is being made
     * @param
     * @return
     * @throws Exception
     */
    public int getMainTaskID(String taskName, String categoryName) throws Exception {
        DatabaseConnection connection = new DatabaseConnection();
        Connection connectDB = connection.getConnection();
        categoryDao = new JdbcCategoryDao();


        try {
            PreparedStatement ps = connectDB.prepareStatement("SELECT * FROM maintask WHERE title=? AND categoryID=?");
            ps.setString(1, taskName);
            ps.setInt(2, categoryDao.getCategoryID(categoryName));
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt("maintaskID");
            }else {
                throw new Exception("This category does not have a task named '" + taskName);
            }
        } catch (SQLException e) {
            throw new Exception("Unable to get task with this name '" + taskName + "': " + e.getMessage());
        } finally {
            connection.closeConnection();
        }
    }

    /**
     * A method that gets all unfinished tasks for database
     * @return a list of the unfinished tasks
     * @throws Exception if unable to get the tasks
     */
    public ObservableList<MainTask> getAllUnfinishedTasks() throws Exception {
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();

        ObservableList<MainTask> mainTasks = FXCollections.observableArrayList();
        try{
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM maintask WHERE done=false");
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                mainTasks.add((new MainTask(rs.getString("title"),
                        LocalDate.parse(rs.getString("deadline")), rs.getInt("priority"),
                        rs.getString("stringOfSubTask"),rs.getString("description"), rs.getBoolean("done"),
                        LocalDate.parse(rs.getString("startdate")), null, rs.getInt("maintaskID"))));
            }
            return mainTasks;
        } catch (SQLException e) {
            throw new Exception("Unable to get categories for user " + e.getMessage());
        } finally {
            databaseConnection.closeConnection();
        }
    }

    /**
     * A method that gets all finished tasks
     * @return a list of the finished tasks
     */
    public ObservableList<MainTask> getAllFinishedTasks() {
        ObservableList<MainTask> mainTasks = FXCollections.observableArrayList();
        try{
            ObservableList<MainTask> allMainTasks = loadMainTasks();
            for(MainTask mainTask : allMainTasks){
                if(mainTask.isDone()){
                    mainTasks.add(mainTask);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }return mainTasks;

    }

    /**
     * A method that gets all finished tasks
     * @return a list of the unfinished tasks
     */
    public ObservableList<MainTask> getAllUnfinishedTasks2() {
        ObservableList<MainTask> mainTasks = FXCollections.observableArrayList();
        try{
            ObservableList<MainTask> allMainTasks = loadMainTasks();
            for(MainTask mainTask : allMainTasks){
                if(!mainTask.isDone()){
                    mainTasks.add(mainTask);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }return mainTasks;

    }

}
