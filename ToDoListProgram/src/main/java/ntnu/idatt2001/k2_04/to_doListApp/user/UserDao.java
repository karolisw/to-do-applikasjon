package ntnu.idatt2001.k2_04.to_doListApp.user;

import javafx.collections.ObservableList;
import ntnu.idatt2001.k2_04.to_doListApp.category.Category;
import java.util.ArrayList;

/**
 * A Dao interface for User
 */
public interface UserDao {
    int getUserID(String username) throws Exception;
    boolean verifyLogin(String username, String password) throws Exception;
    //boolean addNewUser(String firstname, String lastname, String username, String password) throws Exception;
    ObservableList<Category> allCategoriesForUser(int userID) throws Exception;
    void addUserToDB(String firstname, String lastname, String username, String password, byte[] salt) throws Exception;
    User getUser(String username) throws Exception;
    void setDefaultCategoriesNewUser();

}
