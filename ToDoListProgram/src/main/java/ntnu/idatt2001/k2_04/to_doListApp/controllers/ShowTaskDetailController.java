package ntnu.idatt2001.k2_04.to_doListApp.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import ntnu.idatt2001.k2_04.to_doListApp.DatabaseConnection;
import ntnu.idatt2001.k2_04.to_doListApp.mainTask.MainTaskInfo;
import ntnu.idatt2001.k2_04.to_doListApp.SceneChanger;
import ntnu.idatt2001.k2_04.to_doListApp.category.CategoryInfo;
import ntnu.idatt2001.k2_04.to_doListApp.mainTask.JdbcMainTaskDao;
import ntnu.idatt2001.k2_04.to_doListApp.mainTask.MainTask;
import ntnu.idatt2001.k2_04.to_doListApp.mainTask.MainTaskDao;
import ntnu.idatt2001.k2_04.to_doListApp.subTask.JdbcSubTaskDao;
import ntnu.idatt2001.k2_04.to_doListApp.subTask.SubTask;
import ntnu.idatt2001.k2_04.to_doListApp.subTask.SubTaskDao;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ResourceBundle;

public class ShowTaskDetailController implements Initializable {
    private SceneChanger sceneChanger = new SceneChanger();
    private MainTaskDao mainTaskDao = new JdbcMainTaskDao();
    private SubTaskDao subTaskDao = new JdbcSubTaskDao();


    /**
     * Text labels, fields and area
     */
    @FXML
    private Label taskNameLabel;
    @FXML
    private Label categoryLabel;
    @FXML
    private Label deadlineLabel;
    @FXML
    private Label priorityLabel;
    @FXML
    private Label startDateLabel;


    @FXML
    private TextField categoryField;
    @FXML
    private DatePicker deadlineDatePicker;
    @FXML
    private DatePicker startDatePicker;
    @FXML
    private TextArea descriptionArea;
    @FXML
    private TextField taskNameField;
    @FXML
    private ComboBox<Integer> priorityComboBox;

    /**
     * Buttons
     */
    @FXML
    private Button addSubtaskButton;
    @FXML
    private Button deleteSubtaskButton;
    @FXML
    private Button backToHomeButton;
    @FXML
    private Button saveChangesButton;
    @FXML
    private Button editTaskButton;


    /**
     * Tableview
     */
    @FXML
    private TableView<SubTask> tableView;
    @FXML
    private TableColumn<SubTask, String> subTaskNameColumn;
    @FXML
    private TableColumn<SubTask, Integer> subTaskPriorityColumn;
    @FXML
    private TableColumn<SubTask, LocalDate> subTaskDeadlineColumn;
    @FXML
    private TableColumn<SubTask, CheckBox> subTaskCheckBoxColumn;

    /**
     * A method for showing a task in the "show task detail" page
     */
    public void showTask() {
        try {
            taskNameField.setText(MainTaskInfo.getMainTask().getTaskName());
            priorityComboBox.setValue(MainTaskInfo.getMainTask().getTaskPriority());
            categoryField.setText(CategoryInfo.getCategory().getCategoryName());
            deadlineDatePicker.setValue(MainTaskInfo.getMainTask().getDeadline());
            startDatePicker.setValue(MainTaskInfo.getMainTask().getStartDate());
            descriptionArea.setText(MainTaskInfo.getMainTask().getDescription());
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    /**
     * A method for when add subtask button is clicked. it changes the page to add subtask page.
     *
     * @param event
     * @throws IOException
     */
    @FXML
    public void addSubTaskButtonClicked(ActionEvent event) throws IOException {

        sceneChanger.changeScene(event, "addSubTask.fxml", "New sub-task");
    }


    /**
     * A method for taking the user back to home page from the
     * "show task detail" page. Will be called when the button
     * "Back to Home page" is clicked
     */

    @FXML
    public void backToHomeButtonClicked(ActionEvent event) throws IOException {
        MainTaskInfo.removeMainTask();
        sceneChanger.changeScene(event, "Home.fxml", "Home");
    }

    @FXML
    public void disableFields() {
        priorityComboBox.setDisable(true);
        taskNameField.setDisable(true);
        categoryField.setDisable(true);
        deadlineDatePicker.setDisable(true);
        startDatePicker.setDisable(true);
        descriptionArea.setDisable(true);
        tableView.setDisable(true);
        saveChangesButton.setDisable(true);
        deleteSubtaskButton.setDisable(true);
    }

    @FXML
    public void enableFields() {
        priorityComboBox.setDisable(false);
        taskNameField.setDisable(false);
        categoryField.setDisable(false);
        deadlineDatePicker.setDisable(false);
        descriptionArea.setDisable(false);
        tableView.setDisable(false);
        saveChangesButton.setDisable(false);
        deleteSubtaskButton.setDisable(false);
        tableView.setEditable(true);
    }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        //Method returns the task the user had chosen to show details of
        showTask();

        //Setting values for ComboBox containing priorities
        ObservableList<Integer> priorities = FXCollections.observableArrayList(1, 2, 3);
        priorityComboBox.setItems(priorities);

        //Until editButton is pushed
        disableFields();

        subTaskNameColumn.setCellValueFactory(new PropertyValueFactory<SubTask, String>("taskName"));
        subTaskDeadlineColumn.setCellValueFactory(new PropertyValueFactory<SubTask, LocalDate>("deadline"));
        subTaskPriorityColumn.setCellValueFactory(new PropertyValueFactory<SubTask, Integer>("taskPriority"));
        subTaskCheckBoxColumn.setCellValueFactory(new PropertyValueFactory<SubTask, CheckBox>("taskDone"));

        //Updating the table to allow user to edit 'Task Name'-column and 'Priority'-column
        subTaskNameColumn.setCellFactory(TextFieldTableCell.forTableColumn());

        //this will allow the user to select multiple tasks a
        tableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        //Load in data
        try {
            tableView.setItems(subTaskDao.getAllSubTasksInMainTask(MainTaskInfo.getMainTaskID()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void editButtonClicked() {
        enableFields();
    }

    //TODO might remove.... might not...
    public boolean didFieldsChange() {
        if (MainTaskInfo.getMainTask().getTaskName().equals(taskNameField.getText()))
            if (MainTaskInfo.getMainTask().getTaskPriority() == (int) priorityComboBox.getValue())
                if (MainTaskInfo.getMainTask().getDescription().equals(descriptionArea.getText()))
                    if (MainTaskInfo.getMainTask().getDeadline().equals(deadlineDatePicker.getValue())) return false;
        return true;
    }


    /**
     * Mark as finished updates subtasks in db (sets enddate to now and done=true)
     *
     * @param event
     * @throws Exception
     */

    @FXML
    public void saveButtonClicked(ActionEvent event) {
        try {
            updateTaskInDB();
            if (getSelectedCheckboxes().size() > 0) {
                markAsFinished();
                //updating maintaskid
                MainTask task = MainTaskInfo.getMainTask();
                task.setId(mainTaskDao.getMainTaskID(task.getTaskName(),CategoryInfo.getCategory().getCategoryName()));
                MainTaskInfo.setMainTask(task);
            }
            sceneChanger.changeScene(event, "Home.fxml", "Home");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Method checks if fields differ from the static maintask object brought into the stage (with MainTaskInfo)
     * If fields differ, the method changes the old table cells with the new ones inside of db
     *
     * @throws Exception
     */
    public void updateTaskInDB() throws Exception {
        if (!taskNameField.getText().equals(MainTaskInfo.getMainTask().getTaskName())) {
            mainTaskDao.updateTaskName(taskNameField.getText());
        }
        if (!deadlineDatePicker.getValue().equals(MainTaskInfo.getMainTask().getDeadline())) {
            mainTaskDao.updateDeadline(deadlineDatePicker.getValue());
        }
        if (!priorityComboBox.getValue().equals(MainTaskInfo.getMainTask().getTaskPriority())) {
            mainTaskDao.updatePriority(priorityComboBox.getValue());
        }
        if (!descriptionArea.getText().equals(MainTaskInfo.getMainTask().getDescription())) {
            mainTaskDao.updateDescription(descriptionArea.getText());
        }
    }

    /**
     * Supporting method for deleteSubTaskButtonClicked()
     *
     * @return only subtasks where checkbox in doneColumn is checked off
     */
    @FXML
    public ObservableList<SubTask> getSelectedCheckboxes() {
        ObservableList<SubTask> subTasksDone = FXCollections.observableArrayList();
        try {
            for (SubTask subTaskSelected : tableView.getItems()) {
                if (subTaskSelected.getTaskDone().isSelected()) {
                    subTasksDone.add(subTaskSelected);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return subTasksDone;
    }

    public void alertBox() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Keep or remove");
        alert.setContentText("Would you like to delete the sub-task(s)?");
        ButtonType yesButton = new ButtonType("Yes", ButtonBar.ButtonData.YES);
        ButtonType cancelButton = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);
        alert.getButtonTypes().setAll(yesButton, cancelButton);
        alert.showAndWait().

                ifPresent(type ->
                {
                    if (type == yesButton) {
                        try {
                            for(SubTask subTask : getSelectedCheckboxes()){
                                subTaskDao.deleteSubTask(subTask);
                            }
                            tableView.getItems().removeAll(getSelectedCheckboxes());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        //TODO MIGHT ADD TO DIFFERENT VIEW
                    }
                });
    }




    /**
     * When the markAsFinishedButton is clicked, this method marks all the selected subtasks
     * as finished, and deleted them from the tableview.
     */
    @FXML
    public void markAsFinished(){
        try {
            for(SubTask subTask : getSelectedCheckboxes()){
                subTaskDao.setSubTaskDone(subTask);
                //TODO få eirin til å ordne dialogboks her der man velger om man vil delete de eller ei (hvis hun vil)
                subTaskDao.deleteSubTask(subTask);
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Method for the event of editing a task name
     */
    @FXML
    public void editTaskName(TableColumn.CellEditEvent editedCell){
        SubTask subTaskSelected = tableView.getSelectionModel().getSelectedItem();
        subTaskSelected.setTaskName(editedCell.getNewValue().toString());
    }

    /**
     * Supporting method for deleteSubTaskButtonClicked()
     * removes selected tasks from table
     */
    public void deleteSubTasks() {
        ObservableList<SubTask> selectedRows, allRows;
        allRows = tableView.getItems();
        selectedRows = getSelectedCheckboxes();
        allRows.removeAll(selectedRows);


    }
    @FXML
    public void deleteSubTaskButtonClicked() throws Exception {
        if(getSelectedCheckboxes().size() > 0){
            for(SubTask subTask : getSelectedCheckboxes()){
                subTaskDao.deleteSubTask(subTask);
            }
            deleteSubTasks();
        }
    }
}
