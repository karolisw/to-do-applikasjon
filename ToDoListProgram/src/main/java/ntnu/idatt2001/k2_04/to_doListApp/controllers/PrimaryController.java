package ntnu.idatt2001.k2_04.to_doListApp.controllers;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.AnchorPane;
import javafx.util.converter.IntegerStringConverter;
import ntnu.idatt2001.k2_04.to_doListApp.*;
import ntnu.idatt2001.k2_04.to_doListApp.category.Category;
import ntnu.idatt2001.k2_04.to_doListApp.category.CategoryDao;
import ntnu.idatt2001.k2_04.to_doListApp.category.CategoryInfo;
import ntnu.idatt2001.k2_04.to_doListApp.category.JdbcCategoryDao;
import ntnu.idatt2001.k2_04.to_doListApp.mainTask.JdbcMainTaskDao;
import ntnu.idatt2001.k2_04.to_doListApp.mainTask.MainTask;
import ntnu.idatt2001.k2_04.to_doListApp.mainTask.MainTaskDao;
import ntnu.idatt2001.k2_04.to_doListApp.mainTask.MainTaskInfo;
import ntnu.idatt2001.k2_04.to_doListApp.user.JdbcUserDao;
import ntnu.idatt2001.k2_04.to_doListApp.user.UserDao;
import ntnu.idatt2001.k2_04.to_doListApp.user.UserInfo;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.Optional;
import java.util.ResourceBundle;

public class PrimaryController implements Initializable{

    private SceneChanger sceneChanger = new SceneChanger();
    private CategoryDao categoryDao = new JdbcCategoryDao();
    private MainTaskDao mainTaskDao = new JdbcMainTaskDao();
    private UserDao userDao = new JdbcUserDao();

    /**
     * Tableview and column instance-variables
     */
    @FXML private TableView<MainTask> tableView;
    @FXML private TableView<MainTask> currentTableView;

    /**
     * Button instance-variables
     */
    @FXML private Button newTaskButton;
    @FXML private Button deleteButton;
    @FXML private Button showDetailsButton;
    @FXML private Button markTasksAsFinishedButton;


    // Categories
    @FXML private TabPane categoriesTabPane;
    @FXML private AnchorPane newCategoryAnchorPane;


    /**
     * This method creates a new table view and sets column names.
     * @return A new table view.
     */
    public TableView<MainTask> createTableview() {
        TableColumn<MainTask, CheckBox> checkBoxColumn = new TableColumn<>("Selected");
        TableColumn<MainTask, String> taskNameColumn = new TableColumn<>("Task name");
        TableColumn<MainTask, LocalDate> deadlineColumn = new TableColumn<>("Deadline");
        TableColumn<MainTask, String> subTaskColumn = new TableColumn<>("Subtasks");
        TableColumn<MainTask, Integer> priorityColumn = new TableColumn<>("Priority");

        checkBoxColumn.setCellValueFactory(new PropertyValueFactory<>("taskDone"));
        taskNameColumn.setCellValueFactory(new PropertyValueFactory<>("taskName"));
        deadlineColumn.setCellValueFactory(new PropertyValueFactory<>("deadline"));
        subTaskColumn.setCellValueFactory(new PropertyValueFactory<>("stringOfSubTask"));
        priorityColumn.setCellValueFactory(new PropertyValueFactory<>("taskPriority"));

        TableView<MainTask> newTableView = new TableView<>();
        newTableView.getColumns().addAll(checkBoxColumn, taskNameColumn,deadlineColumn,
                subTaskColumn,priorityColumn);
        newTableView.setPrefWidth(600);
        newTableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        //Sets width of column of TableView
        checkBoxColumn.setPrefWidth(70);
        taskNameColumn.setPrefWidth(245);
        deadlineColumn.setPrefWidth(85);
        subTaskColumn.setPrefWidth(100);
        priorityColumn.setPrefWidth(100);

        //Makes columns unresizable
        checkBoxColumn.setResizable(false);
        taskNameColumn.setResizable(false);
        deadlineColumn.setResizable(false);
        subTaskColumn.setResizable(false);
        priorityColumn.setResizable(false);

        // Ability to edit task name and priority
        newTableView.setEditable(true);
        taskNameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        priorityColumn.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));

        // Ability to select multiple tasks at once
        newTableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        return newTableView;
    }

    /**
     * A method that sets the current TableView based on the current Tab selected.
     */
    public TableView<MainTask> getCurrentTableView() throws Exception {
        AnchorPane ap = (AnchorPane) categoriesTabPane.getSelectionModel().getSelectedItem().getContent();

        CategoryInfo.setCategory(categoryDao.getCategoryByName(categoriesTabPane.getSelectionModel().getSelectedItem().getText()));
        for(Node node : ap.getChildren()) {
            if (node instanceof TableView) {
                currentTableView = (TableView<MainTask>) node;
            }

        } return currentTableView;
    }

    /**
     * A method that gets the a tableview by category name.
     * @param categoryName the name of the category that has the tableview.
     * @return the tableview that corresponds with the category argument.
     */
    public TableView<MainTask> getSelectedTableView(String categoryName){
        ObservableList<Tab> categoriesTabs = categoriesTabPane.getTabs();
        for(Tab tab : categoriesTabs){
            if(tab.getText().equals(categoryName)){
                AnchorPane ap = (AnchorPane) tab.getContent();
                for(Node node : ap.getChildren()) {
                    if (node instanceof TableView) {
                        return (TableView<MainTask>) node;
                    }
                }
            }
        }
        return null;
    }


    /**
     * Adds a new category tab to the right in the tab row on the main page. Alert will pop
     * up in case of an exception.
     * @throws IllegalArgumentException if a category with the passed name already excists.
     */
    public void addCategoryTabToTabPane(Category category) throws Exception {
        // Creating new tab with an anchor pane
        Tab newTab = new Tab(category.getCategoryName());
        categoriesTabPane.getTabs().add(newTab);
        newCategoryAnchorPane = new AnchorPane();
        newTab.setContent(newCategoryAnchorPane);

        // Creating the table inside anchorpane and adding tasks
        TableView<MainTask> newTableView = createTableview();
        newCategoryAnchorPane.getChildren().add(newTableView);
        newTableView.setItems(getTasksInCategory(category));
    }

    /**
     * A method that first collects the new category's name, then adds the new category to the user and lastly creates
     * a new category tab.
     * @throws IllegalArgumentException if no category name was entered or a category with the entered name already excists.
     */
    @FXML
    public void addCategoryButtonClicked() {
        try {
            //Collect the new category's name
            TextInputDialog newCategoryDialog = new TextInputDialog();
            newCategoryDialog.setTitle("New category");
            newCategoryDialog.setHeaderText(null);
            newCategoryDialog.setGraphic(null);
            newCategoryDialog.setContentText("Category name");
            Optional<String> result = newCategoryDialog.showAndWait();
            String categoryName = result.get();

            categoryDao.addCategory(categoryName);
            Category newCategory = categoryDao.getCategoryByName(categoryName);
            addCategoryTabToTabPane(newCategory);
        }
        catch (Exception exception) {
            Alert equalFoundAlert = new Alert(AlertType.INFORMATION);
            equalFoundAlert.setTitle("Error");
            equalFoundAlert.setHeaderText(null);
            equalFoundAlert.setGraphic(null);
            equalFoundAlert.setContentText(exception.getMessage());
            equalFoundAlert.show();
        }
    }

    /**
     * A method that deletes a category. An alert window will pop up that gives the user the choise between
     * deleting the tasks of the category or moving them to 'uncategorized'.
     * You can't delete 'all' or 'uncategorized'. If this is tried, an alert will pop up.
     */
    @FXML
    public void deleteCategoryButtonClicked()  {
        String currentCategoryName = categoriesTabPane.getSelectionModel().getSelectedItem().getText();

        if(!currentCategoryName.equals("all") && !currentCategoryName.equals("uncategorized")) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setHeaderText(null);
            alert.setTitle("Move tasks");
            alert.setContentText("Do you want to move the tasks for this category to 'Uncategorized'? If not they will be deleted");
            ButtonType moveButton = new ButtonType("Move tasks", ButtonBar.ButtonData.YES);
            ButtonType deleteButton = new ButtonType("Delete tasks", ButtonBar.ButtonData.NO);
            ButtonType cancelButton = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);
            alert.getButtonTypes().setAll(moveButton, deleteButton, cancelButton);
            alert.showAndWait().ifPresent(type -> {
                if (type == moveButton) {
                    moveCategoriesToUncategorized(currentCategoryName);
                } else if (type == deleteButton) {
                    deleteAllMainTasksInCategory(currentCategoryName);
                } else {
                }
            });
        } else {
            Alert equalFoundAlert = new Alert(AlertType.INFORMATION);
            equalFoundAlert.setTitle("Error");
            equalFoundAlert.setHeaderText(null);
            equalFoundAlert.setGraphic(null);
            equalFoundAlert.setContentText("You can not delete 'all' or 'uncategorized' category");
            equalFoundAlert.show();
        }
    }

    /**
     * A help method for deleteCategoryButtonClicked()
     * A method that moves all tasks in a category to 'uncategorized'.
     * @param currentCategoryName the category the tasks will be moved from.
     */
    public void moveCategoriesToUncategorized(String currentCategoryName){
        try {
            ObservableList<MainTask> mainTasks = categoryDao.getAllMainTasksInCategory(categoryDao.getCategoryID(currentCategoryName));
            for(MainTask mainTask : mainTasks){
                getSelectedTableView("uncategorized").getItems().add(mainTask);
            }
            mainTaskDao.changeToUncategorizedForMainTasksInCategory(categoryDao.getCategoryByName(currentCategoryName));
            categoryDao.deleteCategory(currentCategoryName);
            categoriesTabPane.getTabs().remove(categoriesTabPane.getSelectionModel().getSelectedItem());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * A help method for deleteCategoryButtonClicked()
     * A method that deletes tasks in a category.
     * @param currentCategoryName the category the tasks will be deleted from.
     */
    public void deleteAllMainTasksInCategory(String currentCategoryName){
        try {
            ObservableList<MainTask> mainTasks = categoryDao.getAllMainTasksInCategory(categoryDao.getCategoryID(currentCategoryName));
            for(MainTask mainTask : mainTasks){
                getSelectedTableView("all").getItems().remove(mainTask);
            }
            mainTaskDao.deleteAllMainTasksInCategory(categoryDao.getCategoryByName(currentCategoryName));
            categoryDao.deleteCategory(currentCategoryName);
            categoriesTabPane.getTabs().remove(categoriesTabPane.getSelectionModel().getSelectedItem());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @return the currently selected category (by tab)
     * @throws Exception
     */
    public Category getCurrentCategory() throws Exception {
        String categoryName = categoriesTabPane.getSelectionModel().getSelectedItem().getText();
        return categoryDao.getCategoryByName(categoryName);
    }

    /**
     * When this method is called, it will change the scene to
     * scene where new task is constructed
     */
    public void newTaskButtonPushed(ActionEvent event) throws Exception {

        String categoryName = categoriesTabPane.getSelectionModel().getSelectedItem().getText();
        Category currentCategory = categoryDao.getCategoryByName(categoryName);

        CategoryInfo.setCategory(currentCategory);
        sceneChanger.changeScene(event,"addTask.fxml","New task");
    }

    /**
     * Get the current selected task
     * @return MainTask object of the selected task
     */
    public MainTask getSelectedMainTask() throws Exception {
        TableView<MainTask>  thisTableView = getCurrentTableView();
        return currentTableView.getSelectionModel().getSelectedItem();
    }

    /**
     * This method will change the stage from "home page" to show "subtask page" by clicking on "show subtask" button
     * To be able to show the next maintask and the category it "belongs" to, we must set the info-classes
     */
    @FXML
    public void showDetailsButtonClicked(ActionEvent event) {
        try {
            ObservableList<MainTask> list= getSelectedCheckboxes();
            MainTask task = list.get(0);
            task.setId(mainTaskDao.getMainTaskID(task.getTaskName(),getCurrentCategory().getCategoryName()));
            MainTaskInfo.setMainTask(task);

            String categoryName = categoriesTabPane.getSelectionModel().getSelectedItem().getText();
            Category currentCategory = categoryDao.getCategoryByName(categoryName);
            CategoryInfo.setCategory(currentCategory);

            sceneChanger.changeScene(event, "showTaskDetails.fxml", "Task details");
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Marks selected MainTask as done
     * Sets end-date of selected MainTask to now
     * Uses MainTaskInfo to add the task to a static ObservableList of finished tasks
     * This list will be loaded into the view of FinishedTaskController
     * @param //taskDone is the CellEvent of checking a task as done
     */
    @FXML
    public ObservableList<MainTask> getSelectedCheckboxes() {
        ObservableList<MainTask> tasksDone = FXCollections.observableArrayList();

        try{
            TableView<MainTask> currentTableView = getCurrentTableView();
            for(MainTask mainTask : currentTableView.getItems()){
                if(mainTask.getTaskDone().isSelected()){
                    tasksDone.add(mainTask);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return tasksDone;
    }

    /**
     * A method that disables the finish task button when in 'all' category
     */
    @FXML
    public void disableWhenInAll(){
        try {
            for(Tab tab : categoriesTabPane.getTabs()){
                if(tab.getText().equals("all") && tab.isSelected()){
                    markTasksAsFinishedButton.setDisable(true);
                }
            }/**
            if (getCurrentCategory().getCategoryName().equals("all")){
                markTasksAsFinishedButton.setDisable(true);
            } */
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * This method takes the selected task and deletes it from the tableview (not from db)
     * Used when tasks are marked as finished
     */
    @FXML
    public void deleteSelectedMainTasks(){
        ObservableList<MainTask> selectedRows, allRows;
        try{
            allRows = getCurrentTableView().getItems();
            selectedRows = getSelectedCheckboxes();
            allRows.removeAll(selectedRows);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    /**
     * When the markAsFinishedButton is clicked, this method marks all the selected tasks
     * as finished, and deleted them from the tableview.
     * maintaskinfo is because the id of mantaskinfo is used to mark maintask as done in maintaskdao
     */
    @FXML
    public void markAsFinishedButtonClicked(){
        try {
            for(MainTask mainTask : getSelectedCheckboxes()){
                mainTask.setId(mainTaskDao.getMainTaskID(mainTask.getTaskName(),getCurrentCategory().getCategoryName()));
                MainTaskInfo.setMainTask(mainTask);

                mainTaskDao.setDone();
                mainTaskDao.setEndDate();
            }
            deleteSelectedMainTasks();
            MainTaskInfo.removeMainTask();

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * When showFinishedTaskButton clicked, this method
     * changes the scene to finishedTasks.fxml.
     */
    @FXML
    public void showFinishedTasksButtonClicked(ActionEvent event){
        try{
            if(MainTaskInfo.getMainTask() != null){
                MainTaskInfo.removeMainTask();
            }
            sceneChanger.changeScene(event, "finishedTasks.fxml", "Finished tasks");
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Method to remove selected task(s) from tableView
     * Method works by making two ObservableLists
     * Method removes elements in list of selected tasks from list of all tasks
     */
    public void deleteButtonClicked() throws Exception {
        MainTaskInfo.setMainTask(tableView.getSelectionModel().getSelectedItem());
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Delete task");
        alert.setContentText("Are you sure you want to delete the task?");
        ButtonType yesButton = new ButtonType("Yes", ButtonBar.ButtonData.YES);
        ButtonType cancelButton = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);
        alert.getButtonTypes().setAll(yesButton, cancelButton);
        alert.showAndWait().ifPresent(type -> {
            if (type == yesButton) {
                try {
                    mainTaskDao.deleteMainTask(mainTaskDao.getMainTaskID(getCurrentTableView().getSelectionModel().getSelectedItem().getTaskName(), categoriesTabPane.getSelectionModel().getSelectedItem().getText()));
                    getCurrentTableView().getItems().remove(getCurrentTableView().getSelectionModel().getSelectedItem());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * When logoutButton clicked, the user is taken to login.fxml.
     * @throws IOException if error occurrs
     */
    @FXML
    public void logOutButtonClicked(ActionEvent event) throws IOException {
        if(MainTaskInfo.getMainTask() != null){
            MainTaskInfo.removeMainTask();
        }
        UserInfo.logOut();
        sceneChanger.changeScene(event,"login.fxml", "Login");
    }

    /**
     * This method will allow the user to double click on a cell
     * in 'Task Name'-column to edit task name
     */
    @FXML
    public void changeTaskNameCellEvent(TableColumn.CellEditEvent editCell) throws Exception {
        getCurrentTableView();
        Task selectedTask = currentTableView.getSelectionModel().getSelectedItem();
        selectedTask.setTaskName(editCell.getNewValue().toString());
        mainTaskDao.updateTaskName(editCell.getNewValue().toString());
    }

    /**
     * Disables button when user is viewing the "all" category tab.
     */
    @FXML
    public void disableListener(){
        categoriesTabPane.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number number, Number t1) {
                if(categoriesTabPane.getSelectionModel().getSelectedIndex() == 0){
                    markTasksAsFinishedButton.setDisable(true);
                    deleteButton.setDisable(true);
                    showDetailsButton.setDisable(true);
                    newTaskButton.setDisable(true);
                }
                else {
                    markTasksAsFinishedButton.setDisable(false);
                    deleteButton.setDisable(false);
                    showDetailsButton.setDisable(false);
                    newTaskButton.setDisable(false);
                }
            }
        });
    }

    /**
     * This method will allow the user to double click on a cell
     * in 'Priority'-column to edit priority
     */
    public void changePriorityCellEvent(TableColumn.CellEditEvent editCell) throws Exception {
        getCurrentTableView();
        Task selectedTask = currentTableView.getSelectionModel().getSelectedItem();
        selectedTask.setTaskPriority((Integer) editCell.getNewValue());
        mainTaskDao.updatePriority((Integer) editCell.getNewValue());
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        disableListener();

        loadCategoriesWithMainTasks();


    }

    /**
     * Creates tableviews for new tabs and loads tasks in these tableviews.
     */
    public void loadCategoriesWithMainTasks(){
        try {
            ObservableList<Category> categories = userDao.allCategoriesForUser(UserInfo.getUser().getId());
            for(Category category : categories){
                if(category.getCategoryName().equals("all")){
                    Tab newTab = new Tab(category.getCategoryName());
                    categoriesTabPane.getTabs().add(newTab);
                    newCategoryAnchorPane = new AnchorPane();
                    newTab.setContent(newCategoryAnchorPane);

                    TableView<MainTask> newTableView = createTableview();
                    newCategoryAnchorPane.getChildren().add(newTableView);
                    newTableView.setItems(mainTaskDao.getAllUnfinishedTasks2());
                } else{
                    addCategoryTabToTabPane(category);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @return The category's tasks as an ObservableList
     */
    public ObservableList<MainTask> getTasksInCategory(Category category) throws Exception {
        ObservableList<MainTask> tasksObservableList;
        int categoryID = categoryDao.getCategoryID(category.getCategoryName());
        tasksObservableList = categoryDao.getAllMainTasksInCategory(categoryID);
        return tasksObservableList;
    }
}
