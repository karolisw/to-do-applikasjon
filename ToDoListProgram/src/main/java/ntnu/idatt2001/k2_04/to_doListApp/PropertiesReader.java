package ntnu.idatt2001.k2_04.to_doListApp;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * A class for reading properties
 */
public class PropertiesReader {

    /**
     * A method that takes in the path, read the properties of that path
     * and returns them.
     * @throws IOException if not able to load the properties
     */
    public static Properties read(String path) throws IOException {
        try (InputStream is = new FileInputStream(path)) {
            Properties properties = new Properties();
            properties.load(is);
            return properties;
        } catch (IOException e) {
            throw new IOException("Unable to load properties file " + path + ": " + e.getMessage());
        }
    }
}
