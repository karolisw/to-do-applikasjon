package ntnu.idatt2001.k2_04.to_doListApp.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import ntnu.idatt2001.k2_04.to_doListApp.category.CategoryInfo;
import ntnu.idatt2001.k2_04.to_doListApp.mainTask.MainTaskInfo;
import ntnu.idatt2001.k2_04.to_doListApp.SceneChanger;
import ntnu.idatt2001.k2_04.to_doListApp.category.CategoryDao;
import ntnu.idatt2001.k2_04.to_doListApp.category.JdbcCategoryDao;
import ntnu.idatt2001.k2_04.to_doListApp.mainTask.JdbcMainTaskDao;
import ntnu.idatt2001.k2_04.to_doListApp.mainTask.MainTask;
import ntnu.idatt2001.k2_04.to_doListApp.mainTask.MainTaskDao;
import ntnu.idatt2001.k2_04.to_doListApp.subTask.JdbcSubTaskDao;
import ntnu.idatt2001.k2_04.to_doListApp.subTask.SubTaskDao;
import ntnu.idatt2001.k2_04.to_doListApp.user.User;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

/**
 * This controller class controls the addTask.fxml page.
 */
public class AddTaskController implements Initializable{

    //Object-variables
    private User user;
    private SceneChanger sceneChanger = new SceneChanger();
    private MainTaskDao mainTaskDao = new JdbcMainTaskDao();
    private CategoryDao categoryDao = new JdbcCategoryDao();
    private SubTaskDao subTaskDao= new JdbcSubTaskDao();

    //GUI instances
    @FXML private TextField taskNameField;
    @FXML private TextArea taskDescriptionArea;
    @FXML private DatePicker deadlineDate;
    @FXML private ComboBox priorityComboBox;

    //Observablelists for priorities and categories which will be shown in the combo boxes
    ObservableList<Integer> priorityList = FXCollections.observableArrayList(1,2,3);

    //Initializer method initializes the priority and category list for combo boxes.
    public void initialize(URL location, ResourceBundle resources){
        priorityComboBox.setItems(priorityList);
    }

    /**
     * When cancel clicked, this method changes the scene to Home.fxml
     * @param event an ActionEvent on the button
     * @throws IOException if any error occurs, this exception is thrown.
     */
    public void cancelButtonClicked(ActionEvent event) throws IOException {
        sceneChanger.changeScene(event,"Home.fxml","Home");
    }

    /**
     * Method sends data to PrimaryController.java
     * When create button clicked, the MainTask will be sent to the database,
     * from there it will be loaded into the tableview in PrimaryController.
     * @throws Exception when an error is occurring, it throws an exception
     */
    @FXML
    public void createClicked(ActionEvent event) throws Exception {
        try {
            String categoryName = CategoryInfo.getCategory().getCategoryName();
            if(categoryName.equals("all")) {
                categoryName = "uncategorized";
            }
            mainTaskDao.addMainTask(taskNameField.getText(), (Integer)priorityComboBox.getValue(),
                    deadlineDate.getValue(), taskDescriptionArea.getText(),
                    categoryDao.getCategoryID(categoryName));
            if(MainTaskInfo.getMainTask() != null){
                MainTaskInfo.removeMainTask();
            }
            sceneChanger.changeScene(event, "Home.fxml", "Home");
        } catch (IOException ex) {
            System.err.println(ex);
        } catch (IllegalArgumentException e) {
            Alert equalFoundAlert = new Alert(Alert.AlertType.INFORMATION);
            equalFoundAlert.setTitle("Error");
            equalFoundAlert.setHeaderText(null);
            equalFoundAlert.setGraphic(null);
            equalFoundAlert.setContentText(e.getMessage());
            equalFoundAlert.show();
        }
    }
}
