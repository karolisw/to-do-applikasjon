package ntnu.idatt2001.k2_04.to_doListApp.category;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import ntnu.idatt2001.k2_04.to_doListApp.mainTask.MainTask;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.ArrayList;

/**
 * A class representing a Category
 */
public class Category {
    private int categoryID;
    private String categoryName;
    private int userID;

    private ArrayList<MainTask> tasks;
    private ObservableList<MainTask> taskList;

    /**
     * Constructor for creating an object of a Category
     */
    public Category(String categoryName, int categoryID) {
        this.categoryName = categoryName;
        this.categoryID = categoryID;
        taskList = FXCollections.observableArrayList();
    }
    /**
     * Constructor for creating an object of a Category
     */
    public Category(String categoryName) {
        this.categoryName = categoryName;
        taskList = FXCollections.observableArrayList();
    }

    /**
     * Accessor and mutator methods
     */
    public int getCategoryID() {
        return categoryID;
    }

    public String getCategoryName() {
        return categoryName;
    }


    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public ArrayList<MainTask> getTasks() {
        return tasks;
    }

    /**
     * Adds MainTask to the categories list of tasks.
     */
    public void addTask(MainTask task) {
        tasks.add(task);
    }

    /**
     * Deletes a task from a category's list of tasks. If the task is not in the list.
     * @throws IllegalArgumentException If the task is not an element of the list of tasks, an exception is thrown.
     */
    public void deleteTask(MainTask task) throws IllegalArgumentException {
        if (!tasks.contains(task)) {
            throw new IllegalArgumentException("This task is not registered in this category.");
        }
        else {
            tasks.remove(task);
        }
    }


}