package ntnu.idatt2001.k2_04.to_doListApp;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import java.util.ArrayList;
import java.util.Random;

/**
 * This class represents a popup window that gives the user hints of how to use the program.
 * To run use following code: TODO Remove line 15-17 after implementation
 * HintPopup hintPopup = new HintPopup();
 * hintPopup.display();
 */
public class HintPopup {
    private static ArrayList<String> hints = new ArrayList<>();

    /**
     * A methond that displays a popup window with hints.
     */
    public static void display() {
        Alert hintPopup = new Alert(AlertType.INFORMATION);
        hintPopup.setTitle("Hint");
        hintPopup.setHeaderText(null);
        hintPopup.setContentText(getRandomHint(hints));

        // Graphic in window
        ImageView lightBulb = new ImageView(new Image("lightBulb.png"));
        hintPopup.setGraphic(lightBulb);
        // Icon
        Stage dialogStage = (Stage) hintPopup.getDialogPane().getScene().getWindow();
        dialogStage.getIcons().add(new Image("checkListIcon.png"));

        hintPopup.show();
    }

    /**
     * @return A random hint from the list of hints.
     */
    public static String getRandomHint(ArrayList<String> hints) {
        addHints();
        Random random = new Random();
        return hints.get(random.nextInt(hints.size()));
    }

    /**
     * A help method that adds hints to the hint list.
     */
    public static void addHints() {
        hints.add("Did you know that you can change the priority of a task you have already created?");
        hints.add("Did you know that the finish date is stored when you complete a task? Try clicking " +
                "off a task and check the details of the task.");
        hints.add("Did you know that you can add your own categories? Try the 'Add category' button in " +
                "the left hand corner.");
        hints.add("Did you know can add subtasks to a task? View the details of a task and add them " +
                "from there.");
    }
}
