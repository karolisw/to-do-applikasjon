package ntnu.idatt2001.k2_04.to_doListApp.controllers;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import ntnu.idatt2001.k2_04.to_doListApp.mainTask.JdbcMainTaskDao;
import ntnu.idatt2001.k2_04.to_doListApp.mainTask.MainTaskDao;
import ntnu.idatt2001.k2_04.to_doListApp.mainTask.MainTaskInfo;
import ntnu.idatt2001.k2_04.to_doListApp.SceneChanger;
import ntnu.idatt2001.k2_04.to_doListApp.mainTask.MainTask;
import ntnu.idatt2001.k2_04.to_doListApp.user.UserInfo;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

/**
 * A controller class for the finishedTasks.fxml page.
 */
public class FinishedTasksController implements Initializable {
    private SceneChanger sceneChanger = new SceneChanger();
    private MainTaskDao mainTaskDao = new JdbcMainTaskDao();

    //Buttons
    @FXML
    private Button backToHome;

    //TableView
    @FXML private TableView<MainTask> tableView;
    @FXML private TableColumn<MainTask,String> nameColumn;
    @FXML private TableColumn<MainTask, ComboBox> priorityColumn;
    @FXML private TableColumn<MainTask, LocalDate> deadlineColumn;
    @FXML private TableColumn<MainTask,LocalDate> finishDateColumn;

    /**
     * Initializes the tableview
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        nameColumn.setCellValueFactory(new PropertyValueFactory<MainTask, String>("taskName"));
        priorityColumn.setCellValueFactory(new PropertyValueFactory<MainTask, ComboBox>("taskPriority"));
        deadlineColumn.setCellValueFactory(new PropertyValueFactory<MainTask, LocalDate>("deadline"));
        finishDateColumn.setCellValueFactory(new PropertyValueFactory<MainTask, LocalDate>("endDate"));

        //populateTableView();
        populateTableView();

        //set editable to false
        tableView.setEditable(false);
    }

    /**
     * Fills tableview with finished tasks
     */
    public void populateTableView(){
        try{
            if(UserInfo.getUser() == null){
                tableView.setItems(MainTaskInfo.getFinishedMainTasksNoLogin());
            }else{
                ObservableList<MainTask> list2 = mainTaskDao.getAllFinishedTasks();
                tableView.setItems(list2);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * A method for when the "back to home" button is clicked.
     * It takes user to the "homeWithoutLogin" page.
     */
    @FXML
    public void backToDetails(ActionEvent event){
        try{
            if(UserInfo.getUser() == null) {
                sceneChanger.changeScene(event, "homeWithoutLogin.fxml", "Home");
            }
            sceneChanger.changeScene(event, "Home.fxml", "Home");
        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
