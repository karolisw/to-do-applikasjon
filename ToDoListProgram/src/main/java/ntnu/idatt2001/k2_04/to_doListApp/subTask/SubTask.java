package ntnu.idatt2001.k2_04.to_doListApp.subTask;
import ntnu.idatt2001.k2_04.to_doListApp.Task;

import java.time.LocalDate;

/**
 * Extends Task-class. Makes objects of subtasks.
 */

public class SubTask extends Task {

    private int subtaskID;


    /**
     * A constructor including all subtask object variables.
     */
    public SubTask(String title, int priority, LocalDate deadline, boolean done, LocalDate startDate, LocalDate endDate, int subtaskID) {
        super(title, priority, deadline, done,startDate,endDate,subtaskID);
    }

    /**
     * Acsessor method
     */
    public int getSubtaskID() {
        return subtaskID;
    }

    /**
     * toString method for SubTask
     */
    @Override
    public String toString() {
        return "Subtask name: " + super.getTaskName() + ", priority: " + super.getTaskPriority() + ", deadline: " + super.getDeadline();
    }


}