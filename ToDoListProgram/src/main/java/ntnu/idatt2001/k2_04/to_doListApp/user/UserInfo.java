package ntnu.idatt2001.k2_04.to_doListApp.user;

import ntnu.idatt2001.k2_04.to_doListApp.HintPopup;
import ntnu.idatt2001.k2_04.to_doListApp.user.User;

/**
 * Singleton class
 * To get the current user from anywhere in the app
 */
public class UserInfo {

    /**
     * This object holds the current user once they log in
     */
    private static User user = null;

    /**
     * Constructor hinders creation of objects of this class
     */
    private UserInfo(){
        throw new IllegalStateException("This class cannot be instantiated");
    }

    /**
     * Method that can be used anywhere to get the current user
     */
    public static User getUser(){
        return user;
    }

    /**
     * This method runs once, on successful login.
     * @param DBUser is the user from the database.
     */

    public static void initializeUser(User DBUser){
        //HintPopup.display();
        user = DBUser;
    }

    /**
     * When the user logs out, user-field does no longer contain the user
     * Se
     */
    public static void logOut(){
        user = null;
    }
}
