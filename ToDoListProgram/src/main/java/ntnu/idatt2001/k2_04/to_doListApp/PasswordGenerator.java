package ntnu.idatt2001.k2_04.to_doListApp;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/**
 * A class that generates an encrypted password.
 */
public class PasswordGenerator {

    /**
     * Generates a salt for hashing
     * SecureRandom because it is a strong randomizer (for security)
     * @return a random salt
     */
    public static byte[] generateSalt() throws NoSuchAlgorithmException {
        SecureRandom randomGenerator = SecureRandom.getInstance("SHA1PRNG"); //TODO if this does not works, try "getInstanceStrong();"
        byte[] salt = new byte[16]; //16 byte == 128bits
        randomGenerator.nextBytes(salt);
        return salt;
    }

    /**
     * Method to hash a password with salt
     * @param passwordToEncrypt password to be hashed
     * @param salt salt to use when hashing
     * @return hashedPassword, null if unsuccessful
     */
    public static String hashPassword(String passwordToEncrypt, byte[] salt) throws NoSuchAlgorithmException {
        String generatedPassword;

        try {
            //Get a sha-512 messageDigest
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-512");
            //Adding the salt to messageDigest
            messageDigest.update(salt);
            //Digesting the password. Notice that we are getting the hashed password as an array of bytes.
            byte[] hashedPassword = messageDigest.digest(passwordToEncrypt.getBytes(StandardCharsets.UTF_8));
            //Converting byte array to string
            StringBuilder sb = new StringBuilder();
            for (byte b : hashedPassword) {
                sb.append(Integer.toString((b & 0xff) + 0x100, 16).substring(1));
            }
            generatedPassword = sb.toString();

        } catch (NoSuchAlgorithmException e){
            e.printStackTrace();
            return null;
        }
        return generatedPassword;
    }
}
