package ntnu.idatt2001.k2_04.to_doListApp.mainTask;

import javafx.collections.ObservableList;
import ntnu.idatt2001.k2_04.to_doListApp.category.Category;
import ntnu.idatt2001.k2_04.to_doListApp.user.User;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 * A Dao interface for MainTask
 */
public interface MainTaskDao {
    void addMainTask(String title, int priority, LocalDate deadline, String description, int categoryID) throws Exception;
    void deleteMainTask(int mainTaskID) throws Exception;
    void deleteAllMainTasksInCategory(Category category) throws Exception;
    void changeToUncategorizedForMainTasksInCategory(Category category) throws Exception;
    void updateTaskName(String newTaskName) throws Exception;
    void updatePriority(int newPriority) throws Exception;
    void updateDescription(String description) throws IOException;
    void updateDeadline(LocalDate deadline) throws Exception;
    void setDone() throws IOException;
    ObservableList<MainTask> loadMainTasks() throws IOException;
    ObservableList<MainTask> getAllFinishedTasks();
    ObservableList<MainTask> getAllUnfinishedTasks() throws Exception;
    int getMainTaskID(String taskName, String categoryName) throws Exception;
    ObservableList<MainTask> getAllUnfinishedTasks2();
    void setEndDate()throws Exception;
}
