package ntnu.idatt2001.k2_04.to_doListApp.mainTask;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import ntnu.idatt2001.k2_04.to_doListApp.mainTask.MainTask;

/**
 * Singleton class
 * To get the current mainTask when the user changes scene,
 * and the mainTask is needed after for DB communication
 */
public class MainTaskInfo {

    /**
     * This object holds the current mainTask
     */
    private static MainTask mainTask = null;

    /**
     * This list holds all finished tasks for a user
     */
    private static ObservableList<MainTask> finishedMainTasksNoLogin = FXCollections.observableArrayList();


    private static ObservableList<MainTask> unfinishedMainTasksNoLogin = FXCollections.observableArrayList();


    /**
     * Constructor hinders creation of objects of this class
     */
    private MainTaskInfo(){
        throw new IllegalStateException("This class cannot be instantiated");
    }

    /**
     * Method that can be used anywhere to get the current mainTask
     */
    public static MainTask getMainTask(){
        return mainTask;
    }

    /**
     * This method runs once, on successful login.
     * @param task is the mainTask we wish to keep
     */
    public static void setMainTask(MainTask task){
        mainTask = task;
    }

    /**
     * Adds a finished task to the observable list
     * @param mainTasks is returned when checkboxes are marked as done
     */
    public static void setFinishedMainTasksNoLogin(ObservableList<MainTask> mainTasks){
        finishedMainTasksNoLogin.addAll(mainTasks);
    }

    /**
     * Acsessor methods for getting the observablelists of finished and unfinished tasks.
     */
    public static ObservableList<MainTask> getFinishedMainTasksNoLogin() {
        return finishedMainTasksNoLogin;
    }
    public static ObservableList<MainTask> getUnfinishedMainTasksNoLogin() {
        return unfinishedMainTasksNoLogin;
    }

    public static void addUnfinishedMainTaskNoLogin(MainTask mainTask) {
        unfinishedMainTasksNoLogin.add(mainTask);
    }

    /**
     * When the user logs out, mainTask-field does no longer contain the current mainTask
     */

    public static void removeMainTask(){
        mainTask = null;
    }

    /**
     * Method that returns the ID of the current MainTask
     */
    public static int getMainTaskID(){
        return mainTask.getId();
    }
}