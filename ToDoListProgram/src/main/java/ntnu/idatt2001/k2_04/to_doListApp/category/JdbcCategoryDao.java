package ntnu.idatt2001.k2_04.to_doListApp.category;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import ntnu.idatt2001.k2_04.to_doListApp.DatabaseConnection;
import ntnu.idatt2001.k2_04.to_doListApp.mainTask.MainTask;
import ntnu.idatt2001.k2_04.to_doListApp.user.UserInfo;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

public class JdbcCategoryDao implements CategoryDao {

    /**
     * A method that adds a category
     * @param categoryName the name of the new category
     * @throws Exception if a cateogry with categoryName allready excisits
     */
    public void addCategory(String categoryName) throws Exception {
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();

        try {
            PreparedStatement ps = connection.prepareStatement("SELECT * from category WHERE categoryName=? AND id=?");
            ps.setString(1, categoryName);
            ps.setInt(2, UserInfo.getUser().getId());
            if (!ps.executeQuery().next()) {
                PreparedStatement prepS = connection.prepareStatement("INSERT INTO category(categoryName, id) VALUES (?, ?)");
                prepS.setString(1, categoryName);
                prepS.setInt(2, UserInfo.getUser().getId());
                prepS.execute();
            } else {
                throw new Exception("Category '" + categoryName + "' already exists");
            }
        } catch (SQLException e) {
            throw new Exception("Unable to create category " + e.getMessage());
        } finally{
            databaseConnection.closeConnection();
        }
    }

    /**
     * A method that deletes a category.
     * @param categoryName the name of the category to be deleted
     * @throws Exception if unable to delete category
     */
    public void deleteCategory(String categoryName) throws Exception {
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();

        try {
            PreparedStatement ps = connection.prepareStatement("DELETE FROM category WHERE categoryName=? AND id=?");
            ps.setString(1, categoryName);
            ps.setInt(2, UserInfo.getUser().getId());
            ps.execute();
        } catch (SQLException e) {
            throw new Exception("Unable to delete category with name " + categoryName + ": " + e.getMessage());
        } finally {
            databaseConnection.closeConnection();
        }
    }

    /**
     * A help method that gets category by name
     * @param categoryName the name of the category
     * @return the acutal category object
     * @throws Exception if teh user does not have a category of that name
     */
    public Category getCategoryByName(String categoryName) throws Exception {
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        try {
            PreparedStatement ps = connection.prepareStatement("SELECT * from category WHERE categoryName=? AND id=?");
            ps.setString(1, categoryName);
            ps.setInt(2, UserInfo.getUser().getId());
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return new Category(rs.getString("categoryName"),0);
            } else {
                throw new Exception("This user does not have a category named '" + categoryName + "' does not exist, or the password is wrong.");
            }
        } catch (SQLException e) {
            throw new Exception("Unable to get category with this name '" + categoryName + "': " + e.getMessage());
        } finally {
            databaseConnection.closeConnection();
        }
    }

    /**
     * Method takes in name of category to be found, and the id of the user who owns the category
     * Method is used to set category for a maintask when it is being made
     * @throws Exception if not able to get the name of the category
     */
    public int getCategoryID(String categoryName) throws Exception {
        DatabaseConnection connection = new DatabaseConnection();
        Connection connectDB = connection.getConnection();
        try {
            PreparedStatement ps = connectDB.prepareStatement("SELECT * from category WHERE categoryName=? AND id=?");
            ps.setString(1, categoryName);
            ps.setInt(2, UserInfo.getUser().getId());
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt("categoryID");
            } else {
                throw new Exception("This user does not have a category named '" + categoryName);
            }
        } catch (SQLException e) {
            throw new Exception("Unable to get category with this name '" + categoryName + "': " + e.getMessage());
        } finally {
            connection.closeConnection();
        }
    }

    /**
     * A method that gets all main tasks by categoryID.
     * @return List of tasks in cateogry
     * @throws Exception if unable to get tasks in category
     */
    public ObservableList<MainTask> getAllMainTasksInCategory(int categoryID) throws Exception{
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        ObservableList<MainTask> mainTasksInCategory = FXCollections.observableArrayList();

        try {
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM maintask WHERE categoryID=?");
            ps.setInt(1, categoryID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                    LocalDate enddate;
                if(rs.getString("enddate") == null){
                    enddate = null;
                } else {
                    enddate = LocalDate.parse(rs.getString("enddate"));
                }
                mainTasksInCategory.add(new MainTask(rs.getString("title"),
                        LocalDate.parse(rs.getString("deadline")), rs.getInt("priority"),
                        rs.getString("stringOfSubTask"),rs.getString("description"), rs.getBoolean("done"),
                        LocalDate.parse(rs.getString("startdate")), enddate, rs.getInt("maintaskID")));
            }
        } catch (SQLException e) {
            throw new Exception("Unable to get maintasks in category " + e.getMessage());
        } finally {
            databaseConnection.closeConnection();
        }
        return mainTasksInCategory;
    }

    /**
     * A method that gets all main tasks by categoryID.
     * @return List of tasks in cateogry
     * @throws Exception if unable to get tasks in category
     */
    public ArrayList<MainTask> getAllMainTaskDoneInCategory() throws IOException {
        DatabaseConnection connection = new DatabaseConnection();
        Connection connectDB = connection.getConnection();
        ArrayList<MainTask> doneMainTasks = new ArrayList<>();
        try {
            PreparedStatement ps = connectDB.prepareStatement("SELECT * FROM maintask WHERE done=true AND categoryID=? ");
            ps.setInt(1, CategoryInfo.getCategory().getCategoryID());
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                doneMainTasks.add(new MainTask(rs.getString("title"),
                        LocalDate.parse(rs.getString("deadline")), rs.getInt("priority"),
                        rs.getString("stringOfSubTask"),rs.getString("description"), rs.getBoolean("done"),
                        LocalDate.parse(rs.getString("startDate")), LocalDate.parse(rs.getString("endDate")), rs.getInt("ID")));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            connection.closeConnection();
        }
        return doneMainTasks;
    }
}
