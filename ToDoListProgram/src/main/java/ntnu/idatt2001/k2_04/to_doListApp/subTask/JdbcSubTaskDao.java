package ntnu.idatt2001.k2_04.to_doListApp.subTask;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import ntnu.idatt2001.k2_04.to_doListApp.DatabaseConnection;
import ntnu.idatt2001.k2_04.to_doListApp.mainTask.MainTaskInfo;
import ntnu.idatt2001.k2_04.to_doListApp.mainTask.MainTask;
import ntnu.idatt2001.k2_04.to_doListApp.user.User;
import ntnu.idatt2001.k2_04.to_doListApp.user.UserInfo;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * A class that allows a connection to the database to access the information about a MainTask stored there.
 */
public class JdbcSubTaskDao implements SubTaskDao {

    /**
     * A method that gives the fraction of how many tasks are done.
     * @param mainTaskID the MainTask that holds the subtasks.
     * @return A fraction of how many tasks are done. Numerator: Number of finished tasks.
     * Denominator: Number of total subtasks
     * @throws Exception if unable to connect to database
     */
    public String getNumberOfSubTaskForMainTask(int mainTaskID) throws Exception {
        DatabaseConnection connection = new DatabaseConnection();
        Connection connectDB = connection.getConnection();
        int numberOfSubTask = 0;
        int finishedSubTasks = 0;

        try {
            PreparedStatement ps = connectDB.prepareStatement("SELECT * from subtask WHERE maintaskID=?");
            ps.setInt(1, mainTaskID);
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                numberOfSubTask++;
                if(rs.getBoolean("done")){
                    finishedSubTasks++;
                }
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            connection.closeConnection();
        }
        return finishedSubTasks + "/" + numberOfSubTask;
    }

    /**
     * A method that gets all the subtasks in a maintask
     * @param mainTaskID the id of the maintask that holds the subtasks
     * @return a list of the subtasks in the maintask
     * @throws Exception if unable to get subtasks in category
     */
    public ObservableList<SubTask> getAllSubTasksInMainTask(int mainTaskID) throws Exception{
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();

        ObservableList<SubTask> subTasksInCategory = FXCollections.observableArrayList();
        try {
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM subtask WHERE maintaskID=?");
            ps.setInt(1, mainTaskID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                LocalDate enddate;
                if(rs.getString("enddate") == null){
                    enddate = null;
                } else {
                    enddate = LocalDate.parse(rs.getString("enddate"));
                }
                subTasksInCategory.add(new SubTask(rs.getString("title"), rs.getInt("priority"),
                        LocalDate.parse(rs.getString("deadline")), rs.getBoolean("done"),
                        LocalDate.parse(rs.getString("startdate")),
                        enddate, rs.getInt("maintaskID")))
                ;
            }
        } catch (SQLException e) {
            throw new Exception("Unable to get subtasks in category " + e.getMessage());
        } finally {
            databaseConnection.closeConnection();
        } return subTasksInCategory;
    }

    /**
     * A method that creates a subtask
     * @param mainTaskID the maintask it is a subtask of
     * @throws Exception if unable to create subtask
     */
    public void addNewSubTask(String title, int priority, LocalDate deadline, int mainTaskID) throws Exception {
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();

        try {
            PreparedStatement prepS = connection.prepareStatement("INSERT INTO subtask(title, priority, done, deadline, startdate, maintaskID) VALUES (?, ?, ?, ?,?,?)");
            prepS.setString(1, title);
            prepS.setInt(2, priority);
            prepS.setBoolean(3, false);
            prepS.setString(4, deadline.toString());
            prepS.setString(5, (LocalDate.now()).toString());
            prepS.setInt(6, mainTaskID);
            prepS.execute();
            insertSubTaskStringIntoDB(mainTaskID);
        }
        catch (SQLException e) {
            throw new Exception("Unable to create subtask " + e.getMessage());
        }
        finally {
            databaseConnection.closeConnection();
        }
    }

    /**
     * A method that deletes a subtask
     * @param subtask The subtask to be deleted
     * @throws Exception if unable to connect to database
     */
    @Override
    public void deleteSubTask(SubTask subtask) throws Exception{
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();

        try {
            PreparedStatement ps = connection.prepareStatement("DELETE FROM subtask WHERE subtaskID=?");
            ps.setInt(1, subtask.getSubtaskID());
            ps.execute();
        }
        catch (SQLException e) {
            throw new Exception("Unable to delete subtask : " + e.getMessage());
        }
        finally {
            databaseConnection.closeConnection();
        }
    }

    /**
     * A method that deletes all subtasks from a maintask
     * @param maintaskID the ID of the maintask that holds the subtasks to be deleted
     * @throws Exception if unable to connect to database
     */
    @Override
    public void deleteAllSubTaskFromMainTask(int maintaskID) throws Exception{
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();

        try {
            PreparedStatement prepS = connection.prepareStatement("SELECT * FROM subtask WHERE maintaskID=?");
            prepS.setInt(1, maintaskID);
            ResultSet rs = prepS.executeQuery();
            if(rs.next()) {
                PreparedStatement ps = connection.prepareStatement("DELETE FROM subtask WHERE maintaskID=?");
                ps.setInt(1, maintaskID);
                ps.execute();
            }
        }
        catch (SQLException e) {
            throw new Exception("Unable to delete subtask : " + e.getMessage());
        }
        finally {
            databaseConnection.closeConnection();
        }
    }

    /**
     * Inserts the generated string of subtasks into the table in the db
     * @param mainTaskID the maintask that the fraction belongs to
     * @throws Exception if unable to connect to databse
     */
    public void insertSubTaskStringIntoDB(int mainTaskID) throws Exception {
        DatabaseConnection connection = new DatabaseConnection();
        Connection connectDB = connection.getConnection();

        try{
            //PreparedStatement ps = connectDB.prepareStatement("INSERT INTO maintask(stringOfSubTask) VALUES (?) WHERE maintaskID=?");
            PreparedStatement ps = connectDB.prepareStatement("UPDATE maintask SET stringOfSubTask=? WHERE maintaskID=?");
            ps.setString(1, getNumberOfSubTaskForMainTask(mainTaskID));
            ps.setInt(2, mainTaskID);
            ps.executeUpdate();
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        finally {
            connection.closeConnection();
        }
    }

    /**
     * Sets subtask as done
     * @throws IOException if not able to set subtask done.
     */
    public void setSubTaskDone(SubTask subTask) throws IOException {
        DatabaseConnection connection = new DatabaseConnection();
        Connection connectDB = connection.getConnection();

        try {
            User user = UserInfo.getUser();
            MainTask mainTask = MainTaskInfo.getMainTask();
            PreparedStatement ps = connectDB.prepareStatement("UPDATE subtask SET done=true AND enddate=? WHERE maintaskID=? AND subtaskID=?");
            ps.setString(1, String.valueOf(LocalDate.now()));
            ps.setInt(2, mainTask.getId());
            ps.setInt(3,user.getId());
            ps.execute();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connection.closeConnection();
        }
    }
}
