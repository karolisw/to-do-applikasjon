package ntnu.idatt2001.k2_04.to_doListApp;

import java.io.IOException;
import java.sql.*;
import java.util.Properties;

/**
 * A class representing a connection between the application and the database
 */
public class DatabaseConnection {

    //An object of the Connection interface
    private static Connection connection;

    /**
     * This method connects the application to the database,
     * using PropertiesReader and Drivermanadager
     */
    public Connection getConnection() throws IOException {

        Properties dbProps = PropertiesReader.read("db.properties");
       // String driver = dbProps.getProperty("jdbc.driverClassName");
        String url = dbProps.getProperty("jdbc.url");
        String user = dbProps.getProperty("jdbc.user");
        String password = dbProps.getProperty("jdbc.password");

        try {
            //Class.forName(driver);
            connection = DriverManager.getConnection(url, user, password);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return connection;
}

    /**
     * A method closing the connection the between the application and the database
     */
    public void closeConnection(){

        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
