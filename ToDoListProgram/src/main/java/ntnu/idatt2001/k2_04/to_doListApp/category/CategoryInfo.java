package ntnu.idatt2001.k2_04.to_doListApp.category;

import ntnu.idatt2001.k2_04.to_doListApp.category.Category;

public class CategoryInfo {
    private static Category category= null;

    /**
     * Constructor hinders creation of objects of this class
     */
    private CategoryInfo(){
        throw new IllegalStateException("This class cannot be instantiated");
    }

    /**
     * Method that can be used anywhere to get the current category
     */
    public static Category getCategory(){
        return category;
    }

    /**
     * This method runs once.
     * @param DBCategory is the category from the database.
     */

    public static void setCategory(Category DBCategory){
        category = DBCategory;
    }


    /**
     * When the user logs out, user-field does no longer contain the user
     * Se
     */
    public static void logOut(){
        category = null;
    }
}
