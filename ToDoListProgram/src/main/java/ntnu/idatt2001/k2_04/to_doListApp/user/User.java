package ntnu.idatt2001.k2_04.to_doListApp.user;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import ntnu.idatt2001.k2_04.to_doListApp.PasswordGenerator;
import ntnu.idatt2001.k2_04.to_doListApp.category.Category;
import ntnu.idatt2001.k2_04.to_doListApp.category.CategoryDao;
import ntnu.idatt2001.k2_04.to_doListApp.category.JdbcCategoryDao;
import ntnu.idatt2001.k2_04.to_doListApp.mainTask.JdbcMainTaskDao;
import ntnu.idatt2001.k2_04.to_doListApp.mainTask.MainTask;
import ntnu.idatt2001.k2_04.to_doListApp.mainTask.MainTaskDao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 * A class representing a user.
 */
public class User {

    //Objectvariables
    private int Id;
    private String firstName;
    private String lastName;
    private String username;
    private String password;
    private ArrayList<Category> categories;
    private byte[] salt;


    /**
     * Constructor making objects of user
     * @throws NoSuchAlgorithmException if any error is occurring while making an object of user
     */
    public User(String firstName, String lastName, String username, String password, int Id) throws NoSuchAlgorithmException {
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.password = password;
        categories = new ArrayList<>();
        salt = PasswordGenerator.generateSalt();
        this.password = PasswordGenerator.hashPassword(password,salt);
        this.Id = Id;
    }

    /**
     * Constructor for use in communications with db
     * LoginController uses this constructor
     * For easy retrieval of current user in db
     * @param username taken from login input as user-login is verified
     * @param password same as username
     */
    public User(String username, String password){
        this.username = username;
        this.password = password;
    }

    /**
     * Acsessor and mutator methods
     */
    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }

    public byte[] getSalt() {
        return salt;
    }

    public int getId(){
        return Id;
    }
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public ArrayList<Category> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<Category> categories) {
        this.categories = categories;
    }

    /**
     * A method for getting a category
     * @param categoryName The name of the category to be retrieved.
     * @return The category with the corresponding category name.
     */
    public Category getCategory(String categoryName) {
        for (Category category : categories) {
            if (category.getCategoryName().equals(categoryName)) {
                return category;
            }
        }
        return null;
    }

    /**
     * A method that creates a new category and adds it to the user's list of categories.
     * @throws IllegalArgumentException if no name was entered or a category allready has that name.
     */
    public void addCategory(String categoryName) throws IllegalArgumentException{
        if (categoryName.isEmpty() || categoryName.isBlank()) {
            throw new IllegalArgumentException("Please enter a name for the new category.");
        }

        boolean noEqual = true;
        for (Category category: categories) {
            if(category.getCategoryName().equals(categoryName)) {
                noEqual = false;
            }
        }

        if (noEqual) {
            categories.add(new Category(categoryName));
            //TODO Add category to DB
        }
        else {
            throw new IllegalArgumentException("A category already has this name. Please enter a different name.");
        }
    }

    /**
     * deleteCategory method checks if the category is in the register.
     * If the category is in the registers,
     * the method will check if you want to recategorize or delete the tasks, then do it.
     * If it is not in the register, it will throw an exception.
     *
     * @throws IllegalArgumentException
     * @param category the category we want to delete
     * @param recategorize boolean who say if we want to delete tasks or recategorize them to "Uncategorized"
     */
    public void deleteCategory(Category category, boolean recategorize) throws IllegalArgumentException{
        if(categories.contains(category)){
            if (!category.getTasks().isEmpty()) {
                ArrayList<MainTask> tasks = category.getTasks();
                if (recategorize) {
                    int index = categories.indexOf("Uncategorized");
                    for (MainTask task : tasks) {
                        categories.get(index).addTask(task);
                    }
                } else {
                    for (MainTask task : tasks) {
                        category.deleteTask(task);
                    }
                }
            }
            categories.remove(category);
        } else {
            throw new IllegalArgumentException("Category does not exist in the register.");
        }
    }

    /**
     * toString method for user
     */
    @Override
    public String toString() {
        return "User{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", categories=" + categories +
                '}';
    }
}
