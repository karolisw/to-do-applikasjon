package ntnu.idatt2001.k2_04.to_doListApp.mainTask;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.BooleanPropertyBase;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import ntnu.idatt2001.k2_04.to_doListApp.Task;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;



public class MainTask extends Task {

    private int maintaskID;
    private String description;
    private SimpleStringProperty stringOfSubTask;
    private int categoryID;
    



    /**
     * A constructor using SimpleStringProperties
     */
    public MainTask(String taskName, LocalDate deadline, int priority, String stringOfSubTask, String description, boolean done, LocalDate startDate, LocalDate endDate,int maintaskID){
        super(taskName, priority, deadline, done, startDate, endDate, maintaskID);
        this.stringOfSubTask = new SimpleStringProperty(stringOfSubTask);
        this.description = description;
    }


    /**
     * A constructor excluding subtasks and description
     * This constructor extends super (Task) and actually uses SimpleStringProperty
     * and SimpleIntegerPropery
     * For populating tableView in "homeWithoutLogin.fxml"
     */
    public MainTask(String taskName, int priority, LocalDate deadline) {
        super(taskName, priority, deadline);
    }

    public String getStringOfSubTask() {
        return stringOfSubTask.get();
    }

    public void setStringOfSubTask(String stringOfSubTask) {
        this.stringOfSubTask.set(stringOfSubTask);
    }

    public int getMaintaskID() {
        return maintaskID; }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
