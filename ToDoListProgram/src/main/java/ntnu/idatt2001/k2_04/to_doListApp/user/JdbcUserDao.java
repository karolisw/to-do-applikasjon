package ntnu.idatt2001.k2_04.to_doListApp.user;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import ntnu.idatt2001.k2_04.to_doListApp.DatabaseConnection;
import ntnu.idatt2001.k2_04.to_doListApp.category.Category;
import ntnu.idatt2001.k2_04.to_doListApp.category.CategoryDao;
import ntnu.idatt2001.k2_04.to_doListApp.category.JdbcCategoryDao;
import ntnu.idatt2001.k2_04.to_doListApp.mainTask.JdbcMainTaskDao;
import ntnu.idatt2001.k2_04.to_doListApp.mainTask.MainTask;
import ntnu.idatt2001.k2_04.to_doListApp.PasswordGenerator;
import ntnu.idatt2001.k2_04.to_doListApp.mainTask.MainTaskDao;
import ntnu.idatt2001.k2_04.to_doListApp.user.User;
import ntnu.idatt2001.k2_04.to_doListApp.user.UserDao;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

public class JdbcUserDao implements UserDao {

    private CategoryDao categoryDao = new JdbcCategoryDao();
    private MainTaskDao mainTaskDao = new JdbcMainTaskDao();

    /**
     * Method that gets our users id (so that we can find our categoryid and insert it into new task)
     * @throws Exception if not able to get user ID
     */
    public int getUserID(String username) throws Exception {
        DatabaseConnection connection = new DatabaseConnection();
        Connection connectDB = connection.getConnection();

        int id = 0;
        try {
            PreparedStatement ps = connectDB.prepareStatement("SELECT * from users WHERE username=?");
            ps.setString(1, username);
            ResultSet rs = ps.executeQuery();
            rs.next();
            id= rs.getInt("id");
        } catch (SQLException e) {
            throw new Exception("Unable to get user with username '" + username + "': " + e.getMessage());
        }
        finally {
            connection.closeConnection();
        }
        return id;
    }

    /**
     * A method that gets the user based on the username
     * @param username the username of the user
     * @return the user that has the username
     * @throws Exception if unable to reach a user with username
     */
    public User getUser(String username) throws Exception {
        DatabaseConnection connection = new DatabaseConnection();
        Connection connectDB = connection.getConnection();

        User userFromDB;
        try {
            PreparedStatement ps = connectDB.prepareStatement("SELECT * from users WHERE username=?");
            ps.setString(1, username);

            ResultSet rs = ps.executeQuery();
            rs.next();
            userFromDB= new User(rs.getString("firstname"),rs.getString("lastname"),
                    rs.getString("username"),rs.getString("password"),rs.getInt("id"));
        } catch (SQLException e) {
            throw new Exception("Unable to get user with username '" + username + "': " + e.getMessage());
        }
        finally {
            connection.closeConnection();
        }
        return userFromDB;
    }

    /**
     * Method for verifying user login
     * @throws Exception thrown if no user exists for the given username, the password is wrong  or if a data access error occurs
     * Extracts password and salt from the resultset
     * After the "Blob" is obtained, it is converted back into a byte array
     * Because we now have the original salt, we can encrypt the password input to see if it matches the one in the db
     */
    public boolean verifyLogin(String username, String password) throws Exception {
        DatabaseConnection connectNow = new DatabaseConnection();
        Connection connectDB = connectNow.getConnection();
        ResultSet resultSet;

        int userID = getUserID(username);
        try {
            PreparedStatement ps = connectDB.prepareStatement("SELECT password, salt FROM users WHERE id=?");
            ps.setInt(1, userID);
            resultSet = ps.executeQuery();

            String dbPassword = null;
            byte[] salt = null;
            while (resultSet.next()) {
                dbPassword = resultSet.getString("password");
                Blob blob = resultSet.getBlob("salt");
                int blobLength = (int) blob.length();
                salt = blob.getBytes(1, blobLength);
            }
            //encrypting password with password from parameter (from passwordField)
            String passwordToBeVerified = PasswordGenerator.hashPassword(password, salt);

            //return true if equal. Next step is to set UserInfo as well as change scene to Home.fxml
            if (passwordToBeVerified.equals(dbPassword)) {
                return true;
            }
        }
        catch (SQLException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        finally {
            connectNow.closeConnection();
        }
        return false;
    }

    /**
     * Gets all the categories that belongs to a user
     * by taking the user's ID as parameter.
     * @throws Exception if not able to get all the categories
     */
    public ObservableList<Category> allCategoriesForUser(int userID) throws Exception {
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        ObservableList<Category> categoriesForUser = FXCollections.observableArrayList();
        try{
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM category WHERE id=?");
            ps.setInt(1, userID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                categoriesForUser.add(new Category(rs.getString("categoryName")));
            }
            return categoriesForUser;
        } catch (SQLException e) {
            throw new Exception("Unable to get categories for user " + e.getMessage());
        } finally {
            databaseConnection.closeConnection();
        }
    }

    /**
     * A method thad adds a new user to the database.
     * @param firstname first name of the user
     * @param lastname last name of the user
     * @param username the username (unique)
     * @param password password chosen by user
     * @param salt generated salt
     * @throws Exception if unable to create user
     */
    public void addUserToDB(String firstname, String lastname, String username, String password, byte[] salt) throws Exception{
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();

        try {
            PreparedStatement ps = connection.prepareStatement("INSERT INTO users (firstname,lastname,username,password,salt)"
            + "VALUES (?,?,?,?,?)");

            ps.setString(1, firstname);
            ps.setString(2, lastname);
            ps.setString(3, username);
            ps.setString(4, PasswordGenerator.hashPassword(password, salt));
            ps.setBlob(5, new javax.sql.rowset.serial.SerialBlob(salt));
            ps.execute();

        } catch (SQLException e) {
            throw new Exception("Unable to create user " + e.getMessage());
        }finally {
            databaseConnection.closeConnection();
        }
    }

    /**
     * Sets the deafault categories for a user (when created)
     */
    public void setDefaultCategoriesNewUser() {
        try{
            //add standard categories
            categoryDao.addCategory("all");
            categoryDao.addCategory("school");
            categoryDao.addCategory("work");
            categoryDao.addCategory("personal");
            categoryDao.addCategory("uncategorized");

            //id's of the categories
            int schoolCategoryID= categoryDao.getCategoryID("school");
            int workCategoryID= categoryDao.getCategoryID("work");
            int personalCategoryID= categoryDao.getCategoryID("personal");
            int uncategorizedCategoryID= categoryDao.getCategoryID("uncategorized");

            //adding standard tasks to categories
            mainTaskDao.addMainTask("Do english homework", 2,LocalDate.of(2021,6,23),"Finish assignment",schoolCategoryID);
            mainTaskDao.addMainTask("Call boss", 2,LocalDate.of(2021,5,20),"Phone number: 40404040",workCategoryID);
            mainTaskDao.addMainTask("Visit mom", 2,LocalDate.of(2021,7,1),"It's her birthday",personalCategoryID);
            mainTaskDao.addMainTask("Add new tasks to the To-do List App", 2,LocalDate.of(2021,5,2),"",uncategorizedCategoryID);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
