package ntnu.idatt2001.k2_04.to_doListApp.subTask;

import javafx.collections.ObservableList;
import ntnu.idatt2001.k2_04.to_doListApp.mainTask.MainTask;
import ntnu.idatt2001.k2_04.to_doListApp.subTask.SubTask;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 * A Dao interface for SubTask
 */
public interface SubTaskDao {
    String getNumberOfSubTaskForMainTask(int mainTaskId) throws Exception;
    void addNewSubTask(String title, int priority, LocalDate deadline, int mainTaskId) throws Exception;
    void deleteSubTask(SubTask subtask) throws Exception;
    void deleteAllSubTaskFromMainTask(int maintaskID) throws Exception;
    void insertSubTaskStringIntoDB(int mainTaskId) throws Exception;
    ObservableList<SubTask> getAllSubTasksInMainTask(int mainTaskID) throws Exception;
    void setSubTaskDone(SubTask subTask) throws IOException;
}
