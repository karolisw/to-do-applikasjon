package ntnu.idatt2001.k2_04.to_doListApp.subTask;

import javafx.collections.ObservableList;
import ntnu.idatt2001.k2_04.to_doListApp.category.Category;
import ntnu.idatt2001.k2_04.to_doListApp.category.JdbcCategoryDao;
import ntnu.idatt2001.k2_04.to_doListApp.mainTask.JdbcMainTaskDao;
import ntnu.idatt2001.k2_04.to_doListApp.user.JdbcUserDao;
import ntnu.idatt2001.k2_04.to_doListApp.user.User;
import ntnu.idatt2001.k2_04.to_doListApp.user.UserInfo;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class JdbcSubTaskDaoTest {

    /**
     * Gets the number of subtasks for a maintasks.
     * This user has already 10 subtasks added.
     * The test should therefore write out 0/11,
     * because zero out of 10 subtask is done.
     * @throws Exception if its not able to take out the subtasks number.
     */
    @Test
    void getNumberOfSubTaskForMainTask() throws Exception {
        //Arrange
        JdbcUserDao jdbcUserDao = new JdbcUserDao();
        User user = jdbcUserDao.getUser("kristina2");
        UserInfo.initializeUser(user);
        JdbcMainTaskDao jdbcMainTaskDao = new JdbcMainTaskDao();
        JdbcSubTaskDao jdbcSubTaskDao = new JdbcSubTaskDao();

        //Act
        //jdbcMainTaskDao.addMainTask("testTask",2, LocalDate.of(2021,12,12),"testDesc",101);
        int mainTaskID = jdbcMainTaskDao.getMainTaskID("testTask","Homework");
        //jdbcSubTaskDao.addNewSubTask("testSubTask9",1,LocalDate.now(),mainTaskID);

        String number = jdbcSubTaskDao.getNumberOfSubTaskForMainTask(mainTaskID);

        //Assert
        assertEquals("0/11",number);

    }
}