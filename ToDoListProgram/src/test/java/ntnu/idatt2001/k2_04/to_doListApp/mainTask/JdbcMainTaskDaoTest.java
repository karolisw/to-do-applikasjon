package ntnu.idatt2001.k2_04.to_doListApp.mainTask;

import javafx.collections.ObservableList;
import ntnu.idatt2001.k2_04.to_doListApp.category.CategoryInfo;
import ntnu.idatt2001.k2_04.to_doListApp.category.JdbcCategoryDao;
import ntnu.idatt2001.k2_04.to_doListApp.user.JdbcUserDao;
import ntnu.idatt2001.k2_04.to_doListApp.user.User;
import ntnu.idatt2001.k2_04.to_doListApp.user.UserInfo;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class JdbcMainTaskDaoTest {

    /**
     * Tests addMainTask method in JdbcMainTaskDAO.java
     * Adds a new task to the user "kristina2"'s category "Homework".
     * Tests if the mainTaskID of the newly added mainTask is right,
     * and if it is, means that the MainTask was succesfully added.
     * @throws Exception if the MainTask is already added.
     */
    @Test
    void addMainTask() throws Exception {
        //Arrange
        JdbcUserDao jdbcUserDao = new JdbcUserDao();
        User user = jdbcUserDao.getUser("kristina2");
        UserInfo.initializeUser(user);
        JdbcMainTaskDao jdbcMainTaskDao = new JdbcMainTaskDao();

        //Act
        //jdbcMainTaskDao.addMainTask("newTestTask2",2, LocalDate.of(2021,12,12),"testDesc",101);
        int ID = jdbcMainTaskDao.getMainTaskID("newTestTask2","Homework");
        System.out.println("Maintask ID to the newly created maintask is " + ID);

        //Assert
        assertEquals(285,jdbcMainTaskDao.getMainTaskID("newTestTask2","Homework"));
    }

    /**
     * A nested test inner class for testing deleteMainTask and
     * deleteAllMainTaskInCategory.
     */
    @Nested
    class Delete{

        /**
         * Tests deleteMainTask method in JdbcMainTaskDAO.java.
         * deleteTaskk is already added for the user "kristina2".
         * This tests delete that task and if its deleted successfully
         * it writes out the message "Deleted" to result String.
         * @throws Exception if the MainTask is unable to delete.
         */
        @Test
        void deleteMainTask() throws Exception {
            //Arrange
            JdbcUserDao jdbcUserDao = new JdbcUserDao();
            User user = jdbcUserDao.getUser("kristina2");
            UserInfo.initializeUser(user);
            JdbcMainTaskDao jdbcMainTaskDao = new JdbcMainTaskDao();
            JdbcCategoryDao jdbcCategoryDao = new JdbcCategoryDao();

            //Act
            jdbcMainTaskDao.addMainTask("deleteTaskk",2, LocalDate.of(2021,12,12),"testDesc",101);

            //deleteTaskk ID is 265
            int IDDeleteTask = jdbcMainTaskDao.getMainTaskID("deleteTaskk", "Homework");

            String result = "";
            if(true){
                jdbcMainTaskDao.deleteMainTask(IDDeleteTask);
                result = "Deleted";
            }
            else{
                result = "notDeleted";
            }

            //Assert
            assertEquals("Deleted", result);
        }

        /**
         * Tests deleteAllMainTaskInCategory method in JdbcMainTaskDAO.java
         * Takes an already existing user "kristina" and deletes all its categories.
         * If its able to delete it successfully "Deleted" is written it String result.
         * @throws Exception if its not able to delete all tasks in the category
         */
        @Test
        void deleteAllMainTasksInCategory() throws Exception {
            //Arrange
            JdbcUserDao jdbcUserDao = new JdbcUserDao();
            User user = jdbcUserDao.getUser("kristina2");
            UserInfo.initializeUser(user);
            JdbcMainTaskDao jdbcMainTaskDao = new JdbcMainTaskDao();
            JdbcCategoryDao jdbcCategoryDao = new JdbcCategoryDao();

            //Act
            String result = "";
            if(true) {
                jdbcMainTaskDao.deleteAllMainTasksInCategory(jdbcCategoryDao.getCategoryByName("Homework"));
                result = "deletedAll";
            }
            else{
                result = "failed";
            }

            //Assert
            assertEquals("deletedAll", result);
        }
    }
}