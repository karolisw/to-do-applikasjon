package ntnu.idatt2001.k2_04.to_doListApp.category;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import ntnu.idatt2001.k2_04.to_doListApp.mainTask.JdbcMainTaskDao;
import ntnu.idatt2001.k2_04.to_doListApp.mainTask.MainTask;
import ntnu.idatt2001.k2_04.to_doListApp.user.UserInfo;
import ntnu.idatt2001.k2_04.to_doListApp.user.JdbcUserDao;
import ntnu.idatt2001.k2_04.to_doListApp.user.User;
import ntnu.idatt2001.k2_04.to_doListApp.user.UserInfo;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class JdbcCategoryDaoTest {

    /**
     * Testing addCategory methode in JdbCategoryDao class.
     * Created an user and used initializeUser methode to tell the database that the user is logged inn.
     * Already added the categories "Project" and "Homework" into the user object.
     * Then gets the categories as ObservableList from user and iterates through the list to get the categories name.
     * Tests with assertEqual to check is the added categories were added into the database, by checking if the category names exists in the database.
     */
    @Test
    void addCategory() {
        try {
            //Arrange
            JdbcUserDao jdbcUserDao = new JdbcUserDao();
            JdbcCategoryDao jdbcCategoryDao = new JdbcCategoryDao();
            User user = jdbcUserDao.getUser("kristina2");
            UserInfo.initializeUser(user);
            //Act
            jdbcCategoryDao.addCategory("Project");
            ObservableList<Category> categories = jdbcUserDao.allCategoriesForUser(jdbcUserDao.getUserID(UserInfo.getUser().getUsername()));
            String result = "";
            for(Category category : categories) {
                result += category.getCategoryName() + " ";
            }
            //Assert
            assertEquals("Homework Project ", result);
        }
        catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    /**
     * The test steps in this test methode is almost exactly the same this test,
     * but in this case we  delete a category instead of adding it to the database.
     * @throws Exception
     */
    @Test
    void deleteCategory() throws Exception {
        //Arrange
        JdbcUserDao jdbcUserDao = new JdbcUserDao();
        User user = jdbcUserDao.getUser("kristina2");
        UserInfo.initializeUser(user);
        JdbcCategoryDao jdbcCategoryDao = new JdbcCategoryDao();
        //Act
        jdbcCategoryDao.deleteCategory("Project");
        ObservableList<Category> categories = jdbcUserDao.allCategoriesForUser(jdbcUserDao.getUserID(UserInfo.getUser().getUsername()));
        String result = "";
        for(Category category : categories) {
            result += category.getCategoryName();
        }
        //Assert
        assertEquals("Homework", result);
    }

    /**
     * A nested test inner class, containing tests for getting the category
     * by its name and by its categoryID.
     */
    @Nested
    class getCategory{
        /**
         * Testing getCategoryByName method in JdbcCategoryDAO.java.
         * category "Homework" is already added for the user, and
         * this test gets its name.
         * @throws Exception if the test is not able to collect out the name of the category.
         */
        @Test
        void getCategoryByName() throws Exception {
            //Arrange
            JdbcUserDao jdbcUserDao = new JdbcUserDao();
            JdbcCategoryDao jdbcCategoryDao = new JdbcCategoryDao();
            User user = jdbcUserDao.getUser("kristina2");
            UserInfo.initializeUser(user);

            //Act
            String categoryName = jdbcCategoryDao.getCategoryByName("Homework").getCategoryName();

            //Assert
            assertEquals("Homework",categoryName);
        }

        /**
         * Testing getCategoryID method in JdbcCategoryDAO.java.
         * category "Homework" is already added for the user "kristina2", and
         * this test tries to collect its ID.
         * @throws Exception if the test is not able to collect out the ID of the category.
         */
        @Test
        void getCategoryID() throws Exception {
            //Arrange
            JdbcUserDao jdbcUserDao = new JdbcUserDao();
            JdbcCategoryDao jdbcCategoryDao = new JdbcCategoryDao();
            User user = jdbcUserDao.getUser("kristina2");
            UserInfo.initializeUser(user);

            //Act
            int categoryID = jdbcCategoryDao.getCategoryID("Homework");

            //Assert
            assertEquals(101,categoryID);
        }
    }
}