package ntnu.idatt2001.k2_04.to_doListApp.user;

import javafx.collections.ObservableList;
import ntnu.idatt2001.k2_04.to_doListApp.PasswordGenerator;
import ntnu.idatt2001.k2_04.to_doListApp.category.Category;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class JdbcUserDaoTest {

    /**
     * A nested test inner class containing tests for getting
     * a user object and a userID.
     */
    @Nested
    class getUser{

        /**
         * Testing getUser method in JdbcUserDAO.java.
         * The user "kristina2" is already in the database.
         * This test collects out a user by taking its username as parameter.
         */
        @Test
        void getUserObject() {
            //Arrange
            JdbcUserDao jdbcUserDao = new JdbcUserDao();

            //Act
            String userFirstName = null;
            try {
                userFirstName = jdbcUserDao.getUser("kristina2").getFirstName();
            } catch (Exception e) {
                e.printStackTrace();
            }

            //Assert
            assertEquals("Kristina2", userFirstName);
        }

        /**
         * Testing getUserID method in JdbcUserDAO.java.
         * The user "kristina2" is already in the database and has the userID 10.
         * This test-method tests if getUserID returns integer 10.
         * @throws Exception if the method is not able to collect out the userID.
         */
        @Test
        void getUserID() throws Exception {
            //Arrange
            JdbcUserDao jdbcUserDao = new JdbcUserDao();

            //Act
            int userId = jdbcUserDao.getUserID("kristina2");

            //Assert
            assertEquals(10,userId);
        }
    }

    /**
     * Testing allCategoriesForUser in JdbcUserDAO.java.
     * "Homework" category is already added for the user "kristina2", and is the only category.
     * This test loops through all the categories for the user "kristina2" and collects the category names.
     * @throws Exception if the test is not able to get the categories of the user.
     */
    @Test
    void allCategoriesForUser() throws Exception {
        //Arrange
        JdbcUserDao jdbcUserDao = new JdbcUserDao();

        //Act
        ObservableList<Category> categories = jdbcUserDao.allCategoriesForUser(jdbcUserDao.getUserID("kristina2"));
        String result = "";
        for(Category category : categories) {
            result += category.getCategoryName() + " ";
        }
        //Assert
        assertEquals("Homework Project ", result);

    }

    /**
     * Testing addUserToDB method in JdbcUserDAO.java.
     * Adding a new user and getting the first name of the newly added user
     * to test if the user was successfully added.
     * @throws Exception if it occurs any problem adding a new user to the Database.
     */
    @Test
    void addUserToDB() throws Exception {
        //Arrange
        JdbcUserDao jdbcUserDao = new JdbcUserDao();
        String password = "zara1";
        byte[] salt = new byte[password.length()];

        //Act
        jdbcUserDao.addUserToDB("zara","zara","zara1",password, salt);
        String addedUserFirstName = jdbcUserDao.getUser("zara1").getFirstName();

        //Assert
        assertEquals("zara",addedUserFirstName);
    }
}