# To-do the application

The to-do-list application is developed to help the user to organize and structure their everyday life, both in terms of tasks related to work, school and private life. 

[Link to our wiki pages](https://gitlab.stud.iie.ntnu.no/karolisw/to-do-applikasjon/-/wikis/home)





## Getting started

### Prerequisits
* Git
* Java SDK 11 or newer
* Intellij IDEA or other code editors

### Downloading the project
For getting a copy of our project you will need to clone the repo. This is done by running the following command;

_For https:_
_https://gitlab.stud.idi.ntnu.no/karolisw/to-do-applikasjon.git_


See the [instalation manual](https://gitlab.stud.iie.ntnu.no/karolisw/to-do-applikasjon/-/wikis/Installation%20Manual) and [user manual](https://gitlab.stud.iie.ntnu.no/karolisw/to-do-applikasjon/-/wikis/User%20Manual) for more information on how to run and use the application.





## Authors

Amalie Nystuen - Team leader.

Kristina Ødegård - Meeting organizer and note taker.

Eirin Svinås - Archivist / Document Manager.

Zara Mudassar - Quality assurance, code.

Thadshajini Paramsothy - Quality assurance, documents, UX designer.

Karoline Sund Wahl - QuestionMaster.





## Any questions or do you need help?
Please contact our team leader Amalie Nystuen at email amaliny@stud.ntnu.no
